DROP DATABASE IF EXISTS collegeLibraryDB;
CREATE DATABASE IF NOT EXISTS collegeLibraryDB;
USE collegeLibraryDB;

SELECT 'CREATING DATABASE TABLES' as 'INFO';

/*DROP TABLE IF EXISTS fine;

CREATE TABLE fine (
fine_id INTEGER NOT NULL AUTO_INCREMENT,
f_days_overdue INTEGER,
f_amount DECIMAL (7,2) NOT NULL,
PRIMARY KEY(fine_id)
);*/

DROP TABLE IF EXISTS type;

CREATE TABLE type (
type_id INTEGER NOT NULL AUTO_INCREMENT,
t_type VARCHAR(32) NOT NULL, /*Type: Book or Journal, Administrator or Student*/
PRIMARY KEY(type_id)
);

DROP TABLE IF EXISTS user;

CREATE TABLE user (
user_id INTEGER NOT NULL AUTO_INCREMENT,
u_name VARCHAR(32) NOT NULL,
u_surname VARCHAR(32) NOT NULL,
u_email VARCHAR(32) NOT NULL,
u_password VARCHAR(32) NOT NULL,
type_id INTEGER(11) NOT NULL,
PRIMARY KEY(user_id),
FOREIGN KEY(type_id) REFERENCES type(type_id) /*3 & 4*/
);

DROP TABLE IF EXISTS edition;

CREATE TABLE edition (
edition_id INTEGER NOT NULL AUTO_INCREMENT,
e_isbn VARCHAR(32) NOT NULL, /*ISBN number*/
e_title VARCHAR(32) NOT NULL,
e_author VARCHAR(32) NOT NULL,
e_publisher VARCHAR(32) NOT NULL,
e_date_published DATE NOT NULL,
e_category VARCHAR(32) NOT NULL,
e_quantity_avail INTEGER(11),
e_quantity_total INTEGER(11),
type_id INTEGER(11) NOT NULL, /*Edition type: Book or Journal*/
user_id INTEGER(11) NOT NULL, /*Admin who registered the edition*/
PRIMARY KEY(edition_id),
FOREIGN KEY(type_id) REFERENCES type(type_id),
FOREIGN KEY(user_id) REFERENCES user(user_id)
);

DROP TABLE IF EXISTS request;

CREATE TABLE request (
request_id INTEGER NOT NULL AUTO_INCREMENT,
r_timestamp DATETIME,
edition_id INTEGER NOT NULL,
user_id INTEGER NOT NULL,
PRIMARY KEY(request_id),
FOREIGN KEY(edition_id) REFERENCES edition(edition_id),
FOREIGN KEY(user_id) REFERENCES user(user_id)
);

DROP TABLE IF EXISTS loan;

CREATE TABLE loan (
loan_id INTEGER NOT NULL AUTO_INCREMENT,
l_issue_date DATE NOT NULL,
l_expiry_date DATE NOT NULL,
user_id INTEGER(11) NOT NULL, /*Student who borrows the edition*/
PRIMARY KEY(loan_id),
FOREIGN KEY(user_id) REFERENCES user(user_id) 
);

/*DROP TABLE IF EXISTS fine_charged;

CREATE TABLE fine_charged (
fine_ch_id INTEGER NOT NULL AUTO_INCREMENT,
f_ch_date DATE NOT NULL,

fine_id INTEGER NOT NULL,
loan_id INTEGER NOT NULL,
PRIMARY KEY(fine_ch_id),
FOREIGN KEY(fine_id) REFERENCES fine(fine_id),
FOREIGN KEY(loan_id) REFERENCES loan(loan_id)
);*/

DROP TABLE IF EXISTS item;

CREATE TABLE item (
item_id INTEGER NOT NULL AUTO_INCREMENT,
loan_id INTEGER NOT NULL,
edition_id INTEGER NOT NULL,
PRIMARY KEY(item_id, loan_id),
FOREIGN KEY(loan_id) REFERENCES loan(loan_id),
FOREIGN KEY(edition_id) REFERENCES edition(edition_id)
);

DROP TABLE IF EXISTS inter_edition;

CREATE TABLE inter_edition (
inter_e_id INTEGER NOT NULL AUTO_INCREMENT,
inter_e_isbn VARCHAR(32) NOT NULL,
inter_e_title VARCHAR(32) NOT NULL,
inter_e_author VARCHAR(32) NOT NULL,
inter_e_publisher VARCHAR(32) NOT NULL,
inter_e_date_published DATE NOT NULL,
inter_e_category VARCHAR(32) NOT NULL,
inter_e_quantity_avail INTEGER(11),
inter_e_type VARCHAR(32) NOT NULL,
PRIMARY KEY(inter_e_id)
);

DROP TABLE IF EXISTS inter_request;

CREATE TABLE inter_request (
inter_request_id INTEGER NOT NULL AUTO_INCREMENT,
inter_r_timestamp DATETIME,
inter_e_id INTEGER NOT NULL,
user_id INTEGER NOT NULL,
PRIMARY KEY(inter_request_id),
FOREIGN KEY(inter_e_id) REFERENCES inter_edition(inter_e_id),
FOREIGN KEY(user_id) REFERENCES user(user_id)
);


SELECT 'INSERTING DATA INTO DATABASE' as 'INFO';

INSERT INTO type VALUES(NULL,'Book');
INSERT INTO type VALUES(NULL,'Journal');
INSERT INTO type VALUES(NULL,'Administrator');
INSERT INTO type VALUES(NULL,'Student');
SELECT * FROM type;

INSERT INTO user VALUES(NULL,'Sergejs','Trubacovs','a00212878@student.ait.ie','sergejs',3);
INSERT INTO user VALUES(NULL,'Shane','Walsh','a00211164@student.ait.ie','shane',3);
INSERT INTO user VALUES(NULL,'Brian','Mahon','a00211953@student.ait.ie','brian',3);
INSERT INTO user VALUES(NULL,'Dingcheng','Lu','a00215694@student.ait.ie','dingcheng',3);
INSERT INTO user VALUES(NULL,'Mirza','Munawar','a00226943@student.ait.ie','mirza',3);
INSERT INTO user VALUES(NULL,'Xusheng','Wu','a00215690@student.ait.ie','xusheng',3);
INSERT INTO user VALUES(NULL,'Zhilin','Zhang','a00215642@student.ait.ie','zhilin',3);
INSERT INTO user VALUES(NULL,'Matthew','Thompson','a00007575@student.ait.ie','matthew',4);
INSERT INTO user VALUES(NULL,'Paul','Cordial','a00212975@student.ait.ie','paul',4);
INSERT INTO user VALUES(NULL,'Abdullah','Alshammari','a00191891@student.ait.ie','abdullah',4);
INSERT INTO user VALUES(NULL,'last name','first name','wxs236166@gmail.com','wuxusheng',4);
SELECT * FROM user;

INSERT INTO edition VALUES(NULL,'2853917583245','MySQL RDBMS','John Star','College Publishing',20151020,'Software',5,5,1,1);
INSERT INTO edition VALUES(NULL,'1058362745637','Time','','Time Inc.',20170206,'News',3,3,2,1);
SELECT * FROM edition;

INSERT INTO request VALUES(NULL,NOW(),1,2);
SELECT * FROM request;

/*INSERT INTO fine VALUES(NULL,1,2.00);
INSERT INTO fine VALUES(NULL,2,4.00);
INSERT INTO fine VALUES(NULL,3,6.00);
INSERT INTO fine VALUES(NULL,4,8.00);
INSERT INTO fine VALUES(NULL,5,10.00);
SELECT * FROM fine;*/

INSERT INTO loan VALUES(NULL,20170201,20170215,2);
INSERT INTO loan VALUES(NULL,20170403,20170413,11);
SELECT * FROM loan;

/*INSERT INTO fine_charged VALUES(NULL,20170201,3,1);
SELECT * FROM fine_charged;  */

INSERT INTO item VALUES(NULL,1,2);
/*  SELECT * FROM item;  */

INSERT INTO inter_edition VALUES(NULL,'1058862755637','AJAX','Jack Big','City Publishing',20170328,'Software',2,'Book');
SELECT * FROM inter_edition;

INSERT INTO inter_request VALUES(NULL,NOW(),1,11);
SELECT * FROM inter_request;


SELECT 'CREATING edition_view' as 'INFO:';

DROP VIEW IF EXISTS edition_view;
CREATE VIEW edition_view AS
SELECT edition.edition_id AS ID,
edition.e_isbn AS ISBN,
edition.e_title AS Title,
edition.e_author AS Author,
edition.e_publisher AS Publisher,
edition.e_date_published AS Published,
edition.e_category AS Category,
edition.e_quantity_avail AS Available,
edition.e_quantity_total AS Total,
type.t_type AS Type,
user.u_surname AS Registered_by
FROM edition, user, type
WHERE edition.type_id = type.type_id
AND edition.user_id = user.user_id
ORDER BY edition.edition_id;

SELECT * FROM edition_view;
