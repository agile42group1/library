DROP DATABASE IF EXISTS collegeLibraryDB;
CREATE DATABASE IF NOT EXISTS collegeLibraryDB;
USE collegeLibraryDB;

SELECT 'CREATING DATABASE TABLES' as 'INFO';

DROP TABLE IF EXISTS fine;

CREATE TABLE fine (
fine_id INTEGER NOT NULL AUTO_INCREMENT,
f_days_overdue INTEGER,
f_amount DECIMAL (7,2) NOT NULL,
PRIMARY KEY(fine_id)
);

DROP TABLE IF EXISTS type;

CREATE TABLE type (
type_id INTEGER NOT NULL AUTO_INCREMENT,
t_type VARCHAR(32) NOT NULL, /*Type: Book or Journal, Administrator or Student*/
PRIMARY KEY(type_id)
);

DROP TABLE IF EXISTS user;

CREATE TABLE user (
user_id INTEGER NOT NULL AUTO_INCREMENT,
u_name VARCHAR(32) NOT NULL,
u_surname VARCHAR(32) NOT NULL,
u_email VARCHAR(32) NOT NULL,
u_password VARCHAR(32) NOT NULL,
type_id INTEGER(11) NOT NULL,
PRIMARY KEY(user_id),
FOREIGN KEY(type_id) REFERENCES type(type_id) /*3 & 4*/
);

DROP TABLE IF EXISTS edition;

CREATE TABLE edition (
edition_id INTEGER NOT NULL AUTO_INCREMENT,
e_isbn VARCHAR(32) NOT NULL, /*ISBN number*/
e_title VARCHAR(32) NOT NULL,
e_author VARCHAR(32) NOT NULL,
e_publisher VARCHAR(32) NOT NULL,
e_date_published DATE NOT NULL,
e_category VARCHAR(32) NOT NULL,
e_quantity_avail INTEGER(11),
e_quantity_total INTEGER(11),
type_id INTEGER(11) NOT NULL, /*Edition type: Book or Journal*/
user_id INTEGER(11) NOT NULL,
PRIMARY KEY(edition_id),
FOREIGN KEY(type_id) REFERENCES type(type_id),
FOREIGN KEY(user_id) REFERENCES user(user_id)
);

DROP TABLE IF EXISTS request;

CREATE TABLE request (
request_id INTEGER NOT NULL AUTO_INCREMENT,
r_timestamp DATE,
edition_id INTEGER NOT NULL,
user_id INTEGER NOT NULL,
PRIMARY KEY(request_id),
FOREIGN KEY(edition_id) REFERENCES edition(edition_id),
FOREIGN KEY(user_id) REFERENCES user(user_id)
);


DROP TABLE IF EXISTS notification;
CREATE TABLE notification(
notify_id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
edition_id INTEGER NOT NULL,
user_iD INTEGER NOT NULL,
FOREIGN KEY(user_id) REFERENCES user(user_id),
FOREIGN KEY(edition_id) REFERENCES edition(edition_id)
);

DROP TABLE IF EXISTS loan;

CREATE TABLE loan (
loan_id INTEGER NOT NULL AUTO_INCREMENT,
l_issue_date DATE NOT NULL,
l_expiry_date DATE NOT NULL,
user_id INTEGER(11) NOT NULL, /*Student who borrows the edition*/
PRIMARY KEY(loan_id),
FOREIGN KEY(user_id) REFERENCES user(user_id) 
);

DROP TABLE IF EXISTS fine_charged;

CREATE TABLE fine_charged (
fine_ch_id INTEGER NOT NULL AUTO_INCREMENT,
f_ch_date DATE NOT NULL,

fine_id INTEGER NOT NULL,
loan_id INTEGER NOT NULL,
PRIMARY KEY(fine_ch_id),
FOREIGN KEY(fine_id) REFERENCES fine(fine_id),
FOREIGN KEY(loan_id) REFERENCES loan(loan_id)
);

DROP TABLE IF EXISTS item;

CREATE TABLE item (
item_id INTEGER NOT NULL AUTO_INCREMENT,
loan_id INTEGER NOT NULL,
edition_id INTEGER NOT NULL,
PRIMARY KEY(item_id, loan_id),
FOREIGN KEY(loan_id) REFERENCES loan(loan_id),
FOREIGN KEY(edition_id) REFERENCES edition(edition_id)
);

SELECT 'INSERTING DATA INTO DATABASE' as 'INFO';

INSERT INTO type VALUES(NULL,'Book');
INSERT INTO type VALUES(NULL,'Journal');
INSERT INTO type VALUES(NULL,'Administrator');
INSERT INTO type VALUES(NULL,'Student');
/*  SELECT * FROM type;  */

INSERT INTO user VALUES(NULL,'Sergejs','Trubacovs','trubacovs@gmail.com','secret',3);
INSERT INTO user VALUES(NULL,'Shane','Walsh','shanewalsh1111@gmail.com','terces',4);
/*  SELECT * FROM user;  */

INSERT INTO edition VALUES(NULL,'2853917583245','MySQL RDBMS','John Star','College Publishing',20151020,'Software',5,5,1,1);
INSERT INTO edition VALUES(NULL,'1058362745637','Time','','Time Inc.',20170206,'News',3,3,2,1);
INSERT INTO edition VALUES(NULL,'1058362745632','Time SB1','','Time Inc.',20170206,'News',1,3,2,1);
INSERT INTO edition VALUES(NULL,'1058362745631','Time SB2','','Time Inc.',20170206,'News',0,3,2,1);
/*  SELECT * FROM edition;  */

INSERT INTO request VALUES(NULL,NOW(),1,2);
/*  SELECT * FROM request;  */

INSERT INTO fine VALUES(NULL,1,2.00);
INSERT INTO fine VALUES(NULL,2,4.00);
INSERT INTO fine VALUES(NULL,3,6.00);
INSERT INTO fine VALUES(NULL,4,8.00);
INSERT INTO fine VALUES(NULL,5,10.00);	
/*  SELECT * FROM fine;  */

INSERT INTO loan VALUES(NULL,20170201,20170215,2);
/*  SELECT * FROM loan;  */

INSERT INTO fine_charged VALUES(NULL,20170201,3,1);
/*  SELECT * FROM fine_charged;  */

INSERT INTO item VALUES(NULL,1,2);
/*  SELECT * FROM item;  */

SELECT 'DATA INSERTED INTO DATABASE' as 'INFO';


DROP TABLE IF EXISTS notification_queue;
CREATE TABLE notification_queue(
notify_id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
edition_id INTEGER NOT NULL,
user_iD INTEGER NOT NULL,
FOREIGN KEY(user_id) REFERENCES user(user_id),
FOREIGN KEY(edition_id) REFERENCES edition(edition_id)
);
