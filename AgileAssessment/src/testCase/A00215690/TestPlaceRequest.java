package testCase.A00215690;

import static org.junit.Assert.*;

import java.text.ParseException;

import org.junit.Test;

import exception.UnavailableRequestException;
import exception.WrongRequestException;
import utilise.DAO;

public class TestPlaceRequest {
	/////////////////////////////////////////////////
	// Test Method:processRequest(int editionId,int userId,String
	///////////////////////////////////////////////// issueDate,String
	///////////////////////////////////////////////// expiryDate)
	/////////////////////////////////////////////////

	// invalid test
	// editionId<0
	@Test
	public void test1() {
		try {
			DAO.instance.placeRequestToDB(new int[]{-1}, 2, "2017-2-28", "2017-3-10");
			fail("Should not come here");
		} catch (WrongRequestException e) {
			assertEquals("Wrong Request Exception", e.getMessage());
		} catch (UnavailableRequestException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		}
	}

	// invalid test
	// editionId=0
	@Test
	public void test2() {
		try {
			DAO.instance.placeRequestToDB(new int[]{0}, 2, "2017-2-28", "2017-3-10");
			fail("Should not come here");
		} catch (WrongRequestException e) {
			assertEquals("Wrong Request Exception", e.getMessage());
		} catch (UnavailableRequestException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		}
	}

	// valid test
	// editionId=1
	@Test
	public void test3() {
		try {
			boolean result=DAO.instance.placeRequestToDB(new int[]{1}, 2, "2017-2-28", "2017-3-10");
			assertEquals(true,result);
		} catch (WrongRequestException e) {
			fail("Should not come here");
		} catch (UnavailableRequestException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		}
	}

	// invalid test
	// editionId=100
	@Test
	public void test4() {
		try {
			DAO.instance.placeRequestToDB(new int[]{100}, 2, "2017-2-28", "2017-3-10");
			fail("Should not come here");
		} catch (WrongRequestException e) {
			assertEquals("Wrong Request Exception", e.getMessage());
		} catch (UnavailableRequestException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		}
	}
	
	//valid test
	//the request is not available 
	//editionId=4
	@Test
	public void test5() {
		try {
			DAO.instance.placeRequestToDB(new int[]{4}, 2, "2017-2-28", "2017-3-10");
			fail("Should not come here");
		} catch (WrongRequestException e) {
			fail("Should not come here");
		} catch (UnavailableRequestException e) {
			// TODO Auto-generated catch block
			assertEquals("Unavailable Request Exception", e.getMessage());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		}
	}
	
	// valid test
	// editionId=2
	@Test
	public void test6() {
		try {
			boolean result=DAO.instance.placeRequestToDB(new int[]{2}, 2, "2017-2-28", "2017-3-10");
			assertEquals(true,result);
		} catch (WrongRequestException e) {
			fail("Should not come here");
		} catch (UnavailableRequestException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		}
	}
	
	// invalid test
	// editionId=-5
	@Test
	public void test7() {
		try {
			DAO.instance.placeRequestToDB(new int[]{-5}, 2, "2017-2-28", "2017-3-10");
			fail("Should not come here");
		} catch (WrongRequestException e) {
			assertEquals("Wrong Request Exception", e.getMessage());
		} catch (UnavailableRequestException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		}
	}
	
	// invalid test
	// userId=-5
	@Test
	public void test8() {
		try {
			DAO.instance.placeRequestToDB(new int[]{1}, -5, "2017-2-28", "2017-3-10");
			fail("Should not come here");
		} catch (WrongRequestException e) {
			assertEquals("Wrong Request Exception", e.getMessage());
		} catch (UnavailableRequestException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		}
	}
	// invalid test
	// userId=-1
	@Test
	public void test9() {
		try {
			DAO.instance.placeRequestToDB(new int[]{1}, -1, "2017-2-28", "2017-3-10");
			fail("Should not come here");
		} catch (WrongRequestException e) {
			assertEquals("Wrong Request Exception", e.getMessage());
		} catch (UnavailableRequestException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		}
	}
	// invalid test
	// userId=0
	@Test
	public void test10() {
		try {
			DAO.instance.placeRequestToDB(new int[]{1}, 0, "2017-2-28", "2017-3-10");
			fail("Should not come here");
		} catch (WrongRequestException e) {
			assertEquals("Wrong Request Exception", e.getMessage());
		} catch (UnavailableRequestException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		}
	}
	
	// valid test
	// userId=1
	@Test
	public void test11() {
		try {
			boolean result=DAO.instance.placeRequestToDB(new int[]{1}, 1, "2017-2-28", "2017-3-10");
			assertEquals(true,result);
		} catch (WrongRequestException e) {
			fail("should not come here");
		} catch (UnavailableRequestException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		}
	}
	
	// valid test
	// userId=2
	@Test
	public void test12() {
		try {
			boolean result=DAO.instance.placeRequestToDB(new int[]{1}, 2, "2017-2-28", "2017-3-10");
			assertEquals(true,result);
		} catch (WrongRequestException e) {
			fail("should not come here");
		} catch (UnavailableRequestException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		}
	}
	
	// invalid test
	// userId=100
	@Test
	public void test13() {
		try {
			DAO.instance.placeRequestToDB(new int[]{1}, 100, "2017-2-28", "2017-3-10");
		} catch (WrongRequestException e) {
			assertEquals("Wrong Request Exception", e.getMessage());
		} catch (UnavailableRequestException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		}
	}
	
	// invalid test
	// wrong issue date format
	@Test
	public void test14() {
		try {
			DAO.instance.placeRequestToDB(new int[]{1}, 1, "28-2-2017", "2017-3-10");
		} catch (WrongRequestException e) {
			fail("should not come here");
		} catch (UnavailableRequestException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			assertEquals(e.toString(),e.toString());
		}
	}
	
	// invalid test
	// wrong expiry date format
	@Test
	public void test15() {
		try {
			DAO.instance.placeRequestToDB(new int[]{1}, 1, "2017-2-28", "28-2-2017");
		} catch (WrongRequestException e) {
			fail("should not come here");
		} catch (UnavailableRequestException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			assertEquals(e.toString(),e.toString());
		}
	}
	
	// invalid test
	// issue date less than expiry date
	@Test
	public void test16() {
		try {
			DAO.instance.placeRequestToDB(new int[]{1}, 100, "2017-2-28", "2017-2-26");
		} catch (WrongRequestException e) {
			assertEquals("Wrong Request Exception", e.getMessage());
		} catch (UnavailableRequestException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		}
	}
	
	//valid test
	//two valid edition id
	@Test
	public void test17()
	{
		try {
			boolean result=DAO.instance.placeRequestToDB(new int[]{1,2}, 2, "2017-2-28", "2017-3-10");
			assertEquals(true,result);
		} catch (WrongRequestException e) {
			fail("Should not come here");
		} catch (UnavailableRequestException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			fail("should not come here");
		}
	}
	
}
