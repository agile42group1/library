package testCase.A00215690;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import utilise.DAO;
import utilise.NotificationInfo;

public class TestNotifyExpriyDate {

	
	//TestObj:Valid Test
	//Input: None
	//Output: a Notification Array contains only one object
	//The attribute of the object should be:
	/*
	 * userId:2
	 * userName: Xusheng Wu
	 * email: wxs236166@gmail.com
	 * editionISBN:12345789123455
	 * editionName:12345
	 * ExpriyDate: 2017-4-8
	 * 
	 * */
	
	@Test
	public void test1()
	{
		ArrayList<NotificationInfo> results=DAO.instance.retriExpiryDateNoti();
		for(int i=0;i<results.size();i++)
		{
			NotificationInfo temp=results.get(i);
			int userId=temp.getUserId();
			assertEquals(2,userId);
			String userName=temp.getUserName();
			assertEquals("XUSHENG WU",userName);
			String email=temp.getEmail();
			assertEquals("WXS236166@GMAIL.COM",email);
			String eISBN=temp.geteISBN();
			assertEquals("12345789123455",eISBN);
			String editionName=temp.getEditionName();
			assertEquals("12345",editionName);
			String expiryDate=temp.getExpiryDate();
			assertEquals("2017-04-08",expiryDate);
		}
	}
}
