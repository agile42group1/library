package testCase.A00215690;

import static org.junit.Assert.*;

import org.junit.Test;

import exception.DuplicateISBNException;
import exception.WrongISBNFormatException;
import exception.WrongQuantityNumberException;
import utilise.Edition;
import utilise.DAO;

public class TestUpdateEdition {

	// TestNum:1
	// TestObject:Valid Test
	// Input: edition object (editionId=1, eISBN=1234567891223456 eTitle="test
	// title" author="test author" publisher="test publisher" category="test
	// category" quantityAvail=4 quantityTotal=5 typeId=1 userId=1
	// Expected output:true
	// Actual output:

	@Test
	public void test1() {

		// create a new object and specify their attribute
		Edition updateEdi = new Edition();
		updateEdi.setEditionId(1);
		updateEdi.seteISBN("12345678912345");
		updateEdi.seteTitle("test title");
		updateEdi.setAuthor("test author");
		updateEdi.setDatePublished("2017-3-29");
		updateEdi.setPublisher("test publisher");
		updateEdi.setCategory("test category");
		updateEdi.setQunatityAvail(4);
		updateEdi.setQuantityTotal(5);
		updateEdi.setTypeId(1);
		updateEdi.setUserId(1); // do the test

		boolean result;
		try {
			result = DAO.instance.updateEditionToDB(updateEdi);
			assertEquals(true, result);
		} catch (DuplicateISBNException e) { //
			e.printStackTrace();
			fail("should not come to here");
		} catch (WrongISBNFormatException e) { //
			e.printStackTrace();
			fail("should not come to here");
		} catch (WrongQuantityNumberException e) { //
			e.printStackTrace();
			fail("should not come to here");
		}

	}

	// TestNum:2
	// TestObject:Valid Test
	// Input: edition object (editionId=1, eISBN=1234567891223456 eTitle="test
	// title" author="test author" publisher="test publisher" category="test
	// category" quantityAvail=4 quantityTotal=5 typeId=2 userId=1
	// Expected output:true
	// Actual output:

	@Test
	public void test2() {

		// create a new object and specify their attribute
		Edition updateEdi = new Edition();
		updateEdi.setEditionId(1);
		updateEdi.seteISBN("12345678912344");
		updateEdi.seteTitle("test title");
		updateEdi.setAuthor("test author");
		updateEdi.setDatePublished("2017-3-29");
		updateEdi.setPublisher("test publisher");
		updateEdi.setCategory("test category");
		updateEdi.setQunatityAvail(4);
		updateEdi.setQuantityTotal(5);
		updateEdi.setTypeId(2);
		updateEdi.setUserId(1);

		// do the test
		try {
			boolean result = DAO.instance.updateEditionToDB(updateEdi);
			assertEquals(true, result);
		} catch (DuplicateISBNException e) { // catch block
			e.printStackTrace();
			fail("should not come to here");
		} catch (WrongISBNFormatException e) { // catch block
			e.printStackTrace();
			fail("should not come to here");
		} catch (WrongQuantityNumberException e) { // catch block
			e.printStackTrace();
			fail("should not come to here");
		}

	}

	// TestNum:3
	// TestObject:invalid Test Situation that user type incorrectly make
	// available quantity greater than total
	// Input: edition object (editionId=1, eISBN=1234567891223456 eTitle="test
	// title" author="test author" publisher="test publisher" category="test
	// category" quantityAvail=10 quantityTotal=5 typeId=2 userId=1
	// Expected output:true
	// Actual output:

	@Test
	public void test3() {

		// create a new object and specify their attribute
		Edition updateEdi = new Edition();
		updateEdi.setEditionId(1);
		updateEdi.seteISBN("12345678912343");
		updateEdi.seteTitle("test title");
		updateEdi.setAuthor("test author");
		updateEdi.setDatePublished("2017-3-29");
		updateEdi.setPublisher("test publisher");
		updateEdi.setCategory("test category");
		updateEdi.setQunatityAvail(10);
		updateEdi.setQuantityTotal(5);
		updateEdi.setTypeId(1);
		updateEdi.setUserId(1); // do the test
		try {
			DAO.instance.updateEditionToDB(updateEdi);
			fail("should not come here");
		} catch (

		DuplicateISBNException e) { // catch block
			e.printStackTrace();
			fail("should not come to here");
		} catch (WrongISBNFormatException e) { // catch block
			e.printStackTrace();
			fail("should not come to here");
		} catch (WrongQuantityNumberException e) { // catch block
			e.printStackTrace();
			assertEquals("Wrong quantity number", e.getMessage());
		}
	}

	// TestNum:4
	// TestObject:invalid Test Situation that the eISBN already exists
	// Input: edition object (editionId=1, eISBN=1234567891223456 eTitle="test
	// title" author="test author" publisher="test publisher" category="test
	// category" quantityAvail=10 quantityTotal=5 typeId=2 userId=1
	// Expected output:true
	// Actual output:

	@Test
	public void test4() {

		// create a new object and specify their attribute
		Edition updateEdi = new Edition();
		updateEdi.setEditionId(2);
		updateEdi.seteISBN("12345678912344");
		updateEdi.seteTitle("test title");
		updateEdi.setAuthor("test author");
		updateEdi.setPublisher("test publisher");
		updateEdi.setCategory("test category");
		updateEdi.setQunatityAvail(4);
		updateEdi.setQuantityTotal(5);
		updateEdi.setTypeId(1);
		updateEdi.setUserId(1);

		// do the test
		try {
			DAO.instance.updateEditionToDB(updateEdi);
			fail("should not come here");
		} catch (DuplicateISBNException e) {
			// catch block
			e.printStackTrace();
			assertEquals("Duplicate ISBN", e.getMessage());
		} catch (WrongISBNFormatException e) {
			// catch block
			e.printStackTrace();
			fail("should not come to here");
		} catch (WrongQuantityNumberException e) {
			// catch block
			e.printStackTrace();
			fail("should not come to here");
		}
	}

	// TestNum:5
	// TestObject:invalid Test. The ISBN format is not correct less than 14
	// Input: edition object (editionId=1, eISBN=1234567891223456 eTitle="test
	// title" author="test author" publisher="test publisher" category="test
	// category" quantityAvail=10 quantityTotal=5 typeId=2 userId=1
	// Expected output:true
	// Actual output:
	@Test
	public void test5() {

		// create a new object and specify their attribute
		Edition updateEdi = new Edition();
		updateEdi.setEditionId(1);
		updateEdi.seteISBN("1234567891232");
		updateEdi.seteTitle("test title");
		updateEdi.setAuthor("test author");
		updateEdi.setPublisher("test publisher");
		updateEdi.setDatePublished("2017-3-29");
		updateEdi.setCategory("test category");
		updateEdi.setQunatityAvail(4);
		updateEdi.setQuantityTotal(5);
		updateEdi.setTypeId(1);
		updateEdi.setUserId(1);

		// do the test
		try {
			DAO.instance.updateEditionToDB(updateEdi);
			fail("should not come here");
		} catch (DuplicateISBNException e) {
			// catch block
			fail("should not come to here");
		} catch (WrongISBNFormatException e) {
			// catch block
			// e.printStackTrace();
			assertEquals("Wrong ISBN format, the ISBN should be 14 length number array", e.getMessage());
		} catch (WrongQuantityNumberException e) {
			// catch block
			e.printStackTrace();
			fail("should not come to here");
		}
	}

	// TestNum:6
	// TestObject:invalid Test. The ISBN format is not correct greater than 14
	// Input: edition object (editionId=1, eISBN=1234567891223456 eTitle="test
	// title" author="test author" publisher="test publisher" category="test
	// category" quantityAvail=10 quantityTotal=5 typeId=2 userId=1
	// Expected output:true
	// Actual output:
	@Test
	public void test6() {

		// create a new object and specify their attribute
		Edition updateEdi = new Edition();
		updateEdi.setEditionId(1);
		updateEdi.seteISBN("1234567891234555");
		updateEdi.seteTitle("test title");
		updateEdi.setAuthor("test author");
		updateEdi.setPublisher("test publisher");
		updateEdi.setCategory("test category");
		updateEdi.setQunatityAvail(4);
		updateEdi.setQuantityTotal(5);
		updateEdi.setTypeId(1);
		updateEdi.setUserId(1);

		// do the test
		try {
			DAO.instance.updateEditionToDB(updateEdi);
			fail("should not come here");
		} catch (Exception e) {
			assertEquals("Wrong ISBN format, the ISBN should be 14 length number array", e.getMessage());
		}
	}

}
