package testCase.A00215642;

import junit.framework.TestCase;
import testCase.A00215642.Auth;
import testCase.A00215642.AuthenticationExceptionHandler;

import static org.junit.Assert.*;
import org.junit.Test;

public class Authenticationtest extends TestCase {
	Auth a;
	
	protected void setUp() throws Exception {
		super.setUp();
		a = new Auth();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		a=null;
	}
	
	//Test Id:1
	//Test Case: All valid enter
	//Input: "alan" and "12345"
	//Expect output: successful;
	@Test
	public void test_all_valid() throws Exception {
		try{
			a.login("alan","12345");
		}
		catch(AuthenticationExceptionHandler e){
			assertSame("Successful",  e.getMessage());
		}
	}
	
	//Test Id:2
	//Test Case: All invalid enter
	//Input: "lan" and "123456"
	//Expect output: Login Failed;
	@Test
	public void test_all_invalid() throws Exception {
		try{
			a.login("lan","123456");
		}
		catch(AuthenticationExceptionHandler e){
			assertSame("Login Failed", e.getMessage());
		}
	}
	
	//Test Id:3
	//Test Case: only password valid enter
	//Input: "lan" and "12345"
	//Expect output: Login Failed;
	@Test
	public void test_usernamae_invalid() throws Exception {
		try{
			a.login("lan","12345");
		}
		catch(AuthenticationExceptionHandler e){
			//e.printStackTrace(); 
			assertSame("Login Failed", e.getMessage());
		}
	}
	
	//Test Id:4
	//Test Case: only password invalid enter
	//Input: "alan" and "123456"
	//Expect output: Login Failed;
	@Test
	public void test_pssword_invalid() throws Exception {
		try{
			a.login("alan","123456");
		}
		catch(AuthenticationExceptionHandler e){
			assertEquals("Login Failed", e.getMessage());
		}
	}
	
	//Test Id:5
	//Test Case: empty enter
	//Input: "" and ""
	//Expect output: Login Failed;
	@Test
	public void test_emptyenter() throws Exception {
		try{
			a.login("","");
		}
		catch(AuthenticationExceptionHandler e){
			//e.printStackTrace(); 
			assertSame("Empty enter detected ! Please fill up all fields", e.getMessage());
		}
	}
	
	//Test Id:6
	//Test Case: empty username enter and illegal password enter
	//Input: "" and "./"
	//Expect output: Login Failed;
	@Test
	public void test_emptyusername_wrongtypepassword() throws Exception {
		try{
			a.login("","./");
		}
		catch(AuthenticationExceptionHandler e){
			//e.printStackTrace(); 
			assertSame("Empty enter detected ! Please fill up all fields", e.getMessage());
		}
	}
	
	//Test Id:7
	//Test Case: empty username enter and valid password enter
	//Input: "" and "12345"
	//Expect output: Login Failed;
	@Test
	public void test_emptyusernamae_validpassword() throws Exception {
		try{
			a.login("","12345");
		}
		catch(AuthenticationExceptionHandler e){
			//e.printStackTrace(); 
			assertSame("Empty enter detected ! Please fill up all fields", e.getMessage());
		}
	}
	
	//Test Id:8
	//Test Case: empty username enter and invalid password enter
	//Input: "" and "123456"
	//Expect output: Login Failed;
	@Test
	public void test_emptyusernamae_invalidpassword() throws Exception {
		try{
			a.login("","12456");
		}
		catch(AuthenticationExceptionHandler e){
			//e.printStackTrace(); 
			assertSame("Empty enter detected ! Please fill up all fields", e.getMessage());
		}
	}
	
	//Test Id:9
	//Test Case: illegal username and password enter
	//Input: "./" and "./"
	//Expect output: Login Failed;
	@Test
	public void test_wrongtypeusernamae_password() throws Exception {
		try{
			a.login("./","./");
		}
		catch(AuthenticationExceptionHandler e){
			//e.printStackTrace(); 
			assertSame("Login Failed", e.getMessage());
		}
	}
	
	//Test Id:10
	//Test Case: illegal username enter and valid password enter
	//Input: "./" and "12345"
	//Expect output: Login Failed;
	@Test
	public void test_wrongtypeusernamae_validpassword() throws Exception {
		try{
			a.login("./","12345");
		}
		catch(AuthenticationExceptionHandler e){
			//e.printStackTrace(); 
			assertSame("Login Failed", e.getMessage());
		}
	}
	
	//Test Id:11
	//Test Case: illegal username enter and invalid password enter
	//Input: "./" and "123456"
	//Expect output: Login Failed;
	@Test
	public void test_wrongtypeusernamae_invalidpassword() throws Exception {
		try{
			a.login("./","123456");
		}
		catch(AuthenticationExceptionHandler e){
			//e.printStackTrace(); 
			assertSame("Login Failed", e.getMessage());
		}
	}
	
	//Test Id:12
	//Test Case: illegal username enter and empty password enter
	//Input: "./" and ""
	//Expect output: Login Failed;
	@Test
	public void test_wrongtypeusernamae_emptypassword() throws Exception {
		try{
			a.login("./","");
		}
		catch(AuthenticationExceptionHandler e){
			//e.printStackTrace(); 
			assertSame("Empty enter detected ! Please fill up all fields", e.getMessage());
		}
	}
	
	//Test Id:13
	//Test Case: valid username enter and empty password enter
	//Input: "alan" and ""
	//Expect output: Login Failed;
	@Test
	public void test_validusername_emptypassword() throws Exception {
		try{
			a.login("alan","");
		}
		catch(AuthenticationExceptionHandler e){
			//e.printStackTrace(); 
			assertSame("Empty enter detected ! Please fill up all fields", e.getMessage());
		}
	}
	
	//Test Id:14
	//Test Case: valid username enter and illegal password enter
	//Input: "alan" and "./"
	//Expect output: Login Failed;
	@Test
	public void test_validusername_wrongtypepassword() throws Exception {
		try{
			a.login("alan","./");
		}
		catch(AuthenticationExceptionHandler e){
			//e.printStackTrace(); 
			assertSame("Login Failed", e.getMessage());
		}
	}
	
	//Test Id:15
	//Test Case: invalid username enter and empty password enter
	//Input: "lan" and ""
	//Expect output: Login Failed;
	@Test
	public void test_invalidusername_emptypassword() throws Exception {
		try{
			a.login("lan","");
		}
		catch(AuthenticationExceptionHandler e){
			//e.printStackTrace(); 
			assertSame("Empty enter detected ! Please fill up all fields", e.getMessage());
		}
	}
	
	//Test Id:16
	//Test Case: invalid username enter and illegal password enter
	//Input: "lan" and "./"
	//Expect output: Login Failed;
	@Test
	public void test_invalidusername_wrongtypepassword() throws Exception {
		try{
			a.login("lan","./");
		}
		catch(AuthenticationExceptionHandler e){
			//e.printStackTrace(); 
			assertSame("Login Failed", e.getMessage());
		}
	}
	

}

