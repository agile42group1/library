package testCase.A00215642;

public class AuthenticationExceptionHandler extends Exception {

String message;
	
	public AuthenticationExceptionHandler(String errMessage){
		message = errMessage;
	}
	
	public String getMessage() {
		return message;
	}
}
