package testCase.A00212878;

import static org.junit.Assert.*;

import java.text.ParseException;

import org.junit.Test;

import exception.UnavailableRequestException;
import exception.WrongRequestException;
import utilise.DAO;
import utilise.ResultTableModel;

public class InterlibrarySearchTest {
	
	//TestID: 1
  	//Test Objective: Test valid interlibrary search
  	//Input(s): interlibrary edition isbn or title
	//Valid inputs: isbn=1058862755637, title=AJAX
  	//Expected Output(s): TRUE represents the edition is available

	@Test
	public void interlibrarySearchTest001() {
		String searchKey="SELECT * FROM inter_edition WHERE inter_e_isbn='1058862755637' OR inter_e_title='AJAX'";
		//String interlibrarySearch="SELECT * FROM inter_edition WHERE inter_e_isbn=? or inter_e_title=?";
		try {
			ResultTableModel res = DAO.instance.searchFromDB(searchKey);
			assertEquals(false, res);
		} catch (Exception e) {
			fail("Not yet implemented");
	}	
}
	
	// invalid test
		
		@Test
		public void test1() throws WrongRequestException, UnavailableRequestException, ParseException {
			DAO.instance.searchFromDB("AJAX");
			fail("Should not come here");
		}
	
}