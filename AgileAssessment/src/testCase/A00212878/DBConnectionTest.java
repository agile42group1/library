package testCase.A00212878;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.junit.Assert;
import org.junit.Test;

import utilise.DAO;


public class DBConnectionTest {
	
	//TestID: 1
	//Test Objective: Test valid database connection
	//Input(s): The database url, user name and password. Valid inputs:
  	//			url must be jdbc:mysql://localhost:3306/collegeLibraryDB;
  	//			user name must be root;
  	//			password must be admin;
	//Expected Output(s): TRUE represents the connection is available
    @Test
    public void dbConnectionTest001() throws SQLException {
    	String url="jdbc:mysql://localhost:3307/collegeLibraryDB";
        Connection con = DriverManager.getConnection(url, "root", "admin");
        Assert.assertNotNull(con);
        Assert.assertTrue(con.isValid(0));
        con.close();
    }
    
    //TestID: 2
  	//Test Objective: Test valid database update
  	//Input(s): The database url, user name and password. Valid inputs:
    //			NULL and Newspaper;
  	//Expected Output(s): TRUE represents the update is available
    @Test
	public void dbUpdateTest001() throws SQLException {
			String updateStmt="INSERT INTO type VALUES(NULL,'Newspaper');";
			try {
				boolean res = DAO.instance.executeUpdate(updateStmt);
				assertEquals(true, res);
			} catch (SQLException e) {
				fail("Not yet implemented");
			}
	}
    
}