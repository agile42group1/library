package testCase.A00211164;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.junit.Test;
import junit.framework.TestCase;
import testCase.A00211164.Expire;
import testCase.A00211164.ExpireExceptionHandler;
import testCase.A00211953.CalculateFineExceptionHandler;
import utilise.CalculateFine;

public class ExpireTest extends TestCase {

	SimpleDateFormat returnDate = new SimpleDateFormat("yyyy-MM-dd");

	//Test Id:1
	//Test Case: 1 Day overdue
	//Input: "2017-04-02"
	//Expect output: successful;
	@Test
	public void test_1_1DayOverdue() throws ParseException {

		Expire testObject = new Expire();

		try {
			assertTrue(testObject.Expire(returnDate.parse("2017-03-31")));
		} catch (Exception e) {
			fail("should not come here");
		}
	}

	//Test Id:2
	//Test Case: 2 Days overdue
	//Input: "2017-04-03"
	//Expect output: successful;
	@Test
	public void test_2_2DaysOverdue() throws ParseException {

		Expire testObject = new Expire();

		try {
			assertTrue(testObject.Expire(returnDate.parse("2017-03-30")));
		} catch (Exception e) {
			fail("should not come here");
		}
	}

	//Test Id:3
	//Test Case: 3 Days overdue
	//Input: "2017-04-04"
	//Expect output: successful;
	@Test
	public void test_3_3DaysOverdue() throws ParseException {

		Expire testObject = new Expire();

		try {
			assertTrue(testObject.Expire(returnDate.parse("2017-03-29")));
		} catch (Exception e) {
			fail("should not come here");
		}
	}
	
	//Test Id:4
	//Test Case: 30 Days overdue
	//Input: "2017-03-01"
	//Expect output: successful;
	@Test
	public void test_4_30DaysOverdue() throws ParseException {

		CalculateFine testObject = new CalculateFine();

		try {
			assertTrue(testObject.Calculate(returnDate.parse("2017-03-01")));
		} catch (Exception e) {
			fail("should not come here");
		}
	}

	//Test Id:5
	//Test Case: Return Due Today
	//Input: "2017-04-01"
	//Expect output: successful;
	@Test
	public void test_4_ReturnDueToday() throws ParseException {

		Expire testObject = new Expire();

		try {
			assertTrue(testObject.Expire(returnDate.parse("2017-04-01")));
			fail("Should not get here .. Exception Expected");
		} catch (ExpireExceptionHandler e) {

			assertEquals("A book or Journal is due for return", e.getMessage());
		}
	}

	//Test Id:6
	//Test Case: Return Due 1 Day
	//Input: "2017-04-02"
	//Expect output: successful;
	@Test
	public void test_5_Return1Day() throws ParseException {

		Expire testObject = new Expire();

		try {
			assertTrue(testObject.Expire(returnDate.parse("2017-04-02")));
			fail("Should not get here .. Exception Expected");
		} catch (ExpireExceptionHandler e) {

			assertEquals("Book or Journal not expired", e.getMessage());
		}
	}

	//Test Id:7
	//Test Case: Return Due 2 Days
	//Input: "2017-04-03"
	//Expect output: successful;
	@Test
	public void test_6_ReturnDue2Day() throws ParseException {

		Expire testObject = new Expire();

		try {
			assertTrue(testObject.Expire(returnDate.parse("2017-04-03")));
			fail("Should not get here .. Exception Expected");
		} catch (ExpireExceptionHandler e) {

			assertEquals("Book or Journal not expired", e.getMessage());
		}
	}

	//Test Id:7
	//Test Case: Return Due 3 Days
	//Input: "2017-04-04"
	//Expect output: successful;
	@Test
	public void test_7_ReturnDue3Day() throws ParseException {

		Expire testObject = new Expire();

		try {
			assertTrue(testObject.Expire(returnDate.parse("2017-04-04")));
			fail("Should not get here .. Exception Expected");
		} catch (ExpireExceptionHandler e) {

			assertEquals("Book or Journal not expired", e.getMessage());
		}
	}

	//Test Id:8
	//Test Case: No Expire Date
	//Input: "2017-04-02"
	//Expect output: successful;
	@Test
	public void test_8_NoExpireDate() throws ParseException {

		Expire testObject = new Expire();

		try {
			assertTrue(testObject.Expire(returnDate.parse("2017-04-02")));
			fail("Should not get here .. Exception Expected");
		} catch (ExpireExceptionHandler e) {

			assertEquals("Book or Journal not expired", e.getMessage());
		}
	}

	//Test Id:9
	//Test Case: No Expire Date 2
	//Input: "2017-04-02"
	//Expect output: successful;
	@Test
	public void test_9_NoExpireDate2() throws ParseException {

		Expire testObject = new Expire();

		try {
			assertTrue(testObject.Expire(returnDate.parse("2017-04-02")));
			fail("Should not get here .. Exception Expected");
		} catch (ExpireExceptionHandler e) {

			assertEquals("Book or Journal not expired", e.getMessage());
		}
	}

	//Test Id:10
	//Test Case: No Expire Date 3
	//Input: "2017-05-01"
	//Expect output: successful;
	@Test
	public void test_10_NoExpireDate3() throws ParseException {

		Expire testObject = new Expire();

		try {
			assertTrue(testObject.Expire(returnDate.parse("2017-05-01")));
			fail("Should not get here .. Exception Expected");
		} catch (ExpireExceptionHandler e) {

			assertEquals("Book or Journal not expired", e.getMessage());
		}
	}
}