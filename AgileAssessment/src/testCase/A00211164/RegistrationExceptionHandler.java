package testCase.A00211164;

public class RegistrationExceptionHandler extends Exception {

String message;
	
	public RegistrationExceptionHandler(String errMessage){
		message = errMessage;
	}
	
	public String getMessage() {
		return message;
	}
}