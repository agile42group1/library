package testCase.A00211164;

import junit.framework.TestCase;
import testCase.A00211164.Registration;
import testCase.A00211164.RegistrationExceptionHandler;

import static org.junit.Assert.*;

import org.junit.Test;

public class Registrationtest extends TestCase {
	Registration a;
	
	protected void setUp() throws Exception {
		super.setUp();
		a = new Registration();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		a=null;
	}
	
	//Test Id:1
	//Test Case: All valid entries
	//Input: "john" and "doe" and "doe@gmail.com" and "12345" and "4"
	//Expect output: successful;
	@Test
	public void test_all_valid() throws Exception {
		try{
			a.Register("john","doe","doe@gmail.com","12345","4");
		}
		catch(RegistrationExceptionHandler e){
			assertSame("Successful",  e.getMessage());
		}
	}

	//Test Id:2
	//Test Case: All invalid entries
	//Input: "shane" and "walsh" and "walsh@gmail.com" and "123456" and "3"
	//Expect output: Registration Failed;
	@Test
	public void test_all_invalid() throws Exception {
		try{
			a.Register("shane","walsh","walsh@gmail.com","123456","3");
		}
		catch(RegistrationExceptionHandler e){
			assertSame("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:3
	//Test Case: only fname valid enter
	//Input: "john" and "doe1" and "doe1@gmail.com" and "123456" and "3"
	//Expect output: Registration Failed;
	@Test
	public void test_fname_valid_only() throws Exception {
		try{
			a.Register("john","doe1","doe1@gmail.com","123456","3");
		}
		catch(RegistrationExceptionHandler e){
			assertSame("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:4
	//Test Case: only fname invalid enter
	//Input: "Shane" and "doe" and "doe@gmail.com" and "12345" and "4"
	//Expect output: Registration Failed;
	@Test
	public void test_fname_invalid() throws Exception {
		try{
			a.Register("Shane","doe","doe@gmail.com","12345","4");
		}
		catch(RegistrationExceptionHandler e){
			assertEquals("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:5
	//Test Case: only lname valid enter
	//Input: "shane" and "doe" and "doe1@gmail.com" and "123456" and "3"
	//Expect output: Registration Failed;
	@Test
	public void test_lname_valid_only() throws Exception {
		try{
			a.Register("shane","doe","doe1@gmail.com","123456","3");
		}
		catch(RegistrationExceptionHandler e){
			assertSame("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:6
	//Test Case: only lname invalid enter
	//Input: "john" and "walsh" and "doe@gmail.com" and "12345" and "4"
	//Expect output: Registration Failed;
	@Test
	public void test_lname_invalid_only() throws Exception {
		try{
			a.Register("john","walsh","doe1@gmail.com","123456","5");
		}
		catch(RegistrationExceptionHandler e){
			assertEquals("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:7
	//Test Case: only email valid enter
	//Input: "shane" and "walsh" and "doe@gmail.com" and "123456" and "3"
	//Expect output: Registration Failed;
	@Test
	public void test_email_valid_only() throws Exception {
		try{
			a.Register("shane","walsh","do1@gmail.com","123456","3");
		}
		catch(RegistrationExceptionHandler e){
			//e.printStackTrace(); 
			assertSame("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:8
	//Test Case: only email invalid enter
	//Input: "john" and "doe" and "rob@gmail.com" and "12345" and "4"
	//Expect output: Registration Failed;
	@Test
	public void test_email_invalid_only() throws Exception {
		try{
			a.Register("john","doe","rob@gmail.com","123456","5");
		}
		catch(RegistrationExceptionHandler e){
			assertEquals("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:9
	//Test Case: only password valid enter
	//Input: "shane" and "walsh" and "doe1@gmail.com" and "12345" and "3"
	//Expect output: Registration Failed;
	@Test
	public void test_password_valid_only() throws Exception {
		try{
			a.Register("shane","walsh","doe1@gmail.com","12345","3");
		}
		catch(RegistrationExceptionHandler e){
			//e.printStackTrace(); 
			assertSame("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:10
	//Test Case: only password invalid enter
	//Input: "john" and "doe" and "doe@gmail.com" and "123456" and "5"
	//Expect output: Registration Failed;
	@Test
	public void test_password_invalid_only() throws Exception {
		try{
			a.Register("john","doe","doe@gmail.com","123456","5");
		}
		catch(RegistrationExceptionHandler e){
			assertEquals("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:11
	//Test Case: only type_Id valid enter
	//Input: "shane" and "walsh" and "doe1@gmail.com" and "123456" and "4"
	//Expect output: Registration Failed;
	@Test
	public void test_type_Id_valid_only() throws Exception {
		try{
			a.Register("shane","walsh","doe1@gmail.com","123456","4");
		}
		catch(RegistrationExceptionHandler e){
			//e.printStackTrace(); 
			assertSame("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:12
	//Test Case: only type_Id invalid enter
	//Input: "john" and "doe" and "doe@gmail.com" and "12345" and "6"
	//Expect output: Registration Failed;
	@Test
	public void test_Type_Id_invalid_only() throws Exception {
		try{
			a.Register("john","doe","doe@gmail.com","12345","6");
		}
		catch(RegistrationExceptionHandler e){
			assertEquals("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:13
	//Test Case: enter empty fields
	//Input: "" and "" and "" and "" and "" and ""
	//Expect output: Registration Failed;
	@Test
	public void test_emptyfields() throws Exception {
		try{
			a.Register("","","","","");
		}
		catch(RegistrationExceptionHandler e){
			//e.printStackTrace(); 
			assertSame("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:14
	//Test Case: enter 1 empty field
	//Input: "john" and "doe" and "doe@gmail.com" and "12345" and ""
	//Expect output: Registration Failed;
	@Test
	public void test_valid_data_emptytype_Id() throws Exception {
		try{
			a.Register("john","doe","doe@gmail.com","12345","");
		}
		catch(RegistrationExceptionHandler e){
			assertSame("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:15
	//Test Case: enter empty fname
	//Input: "" and "doe" and "doe@gmail.com" and "12345" and "4");
	//Expect output: Registration Failed;
	@Test
	public void test_valid_data_emptyfname() throws Exception {
		try{
			a.Register("","doe","doe@gmail.com","12345","4");
		}
		catch(RegistrationExceptionHandler e){
			assertSame("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:16
	//Test Case: enter empty fname enter and invalid fname
	//Input: "" and "doe1" and "doe@gmail.com" and "12345" and "4");
	//Expect output: Registration Failed;
	@Test
	public void test_emptyfname_invalidlname() throws Exception {
		try{
			a.Register("","doe1","doe@gmail.com","12345","4");
		}
		catch(RegistrationExceptionHandler e){
			assertSame("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:17
	//Test Case: enter illegal fields
	//Input: "./" and "./" and "./" and "./" and "./");
	//Expect output: Registration Failed;
	@Test
	public void test_invalid_data() throws Exception {
		try{
			a.Register("./","./","./","./","./");
		}
		catch(RegistrationExceptionHandler e){
			assertSame("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:18
	//Test Case: enter illegal fname and valid entries
	//Input: "./" and "doe" and "doe@gmail.com" and "12345" and "4"
	//Expect output: Registration Failed;
	@Test
	public void test_valid_data_invalid_fname() throws Exception {
		try{
			a.Register("./","doe","doe@gmail.com","12345","4");
		}
		catch(RegistrationExceptionHandler e){
			assertSame("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:19
	//Test Case: enter illegal fname enter and invalid lname
	//Input: "/." and "walsh" and "doe@gmail.com" and "12345" and "4"
	//Expect output: Registration Failed;
	@Test
	public void test_invalid_fname_invalid_lname() throws Exception {
		try{
			a.Register("/.","walsh","doe@gmail.com","12345","4");
		}
		catch(RegistrationExceptionHandler e){
			assertSame("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:20
	//Test Case: enter illegal fname and empty lname
	//Input: "./" and "" and "doe@gmail.com" and "12345" and "4"
	//Expect output: Registration Failed;
	@Test
	public void test_invalid_fname_empty_lname() throws Exception {
		try{
			a.Register("./","","doe@gmail.com","12345","4");
		}
		catch(RegistrationExceptionHandler e){
			assertSame("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:21
	//Test Case: valid fname enter and empty lname enter
	//Input: "john" and "" and "doe@gmail.com" and "12345" and "4"
	//Expect output: Registration Failed;
	@Test
	public void test_valid_fname_empty_lname() throws Exception {
		try{
			a.Register("john","","doe@gmail.com","12345","4");
		}
		catch(RegistrationExceptionHandler e){
			assertSame("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:22
	//Test Case: valid fname enter and illegal lname enter
	//Input: "john" and "./" and "doe@gmail.com" and "12345" and "4"
	//Expect output: Registration Failed;
	@Test
	public void test_valid_fname_invalid_lname() throws Exception {
		try{
			a.Register("john","./","doe@gmail.com","12345","4");
		}
		catch(RegistrationExceptionHandler e){
			assertSame("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:23
	//Test Case: invalid fname enter and empty lname enter
	//Input: "john" and "" and "doe@gmail.com" and "12345" and "4"
	//Expect output: Registration Failed;
	@Test
	public void test_invalid_fname_and_empty_lname() throws Exception {
		try{
			a.Register("./","","doe@gmail.com","12345","4");
		}
		catch(RegistrationExceptionHandler e){
			assertSame("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:24
	//Test Case: invalid fnmae enter and illegal lname enter
	//Input: "john" and "./" and "doe@gmail.com" and "12345" and "4"
	//Expect output: Registration Failed;
	@Test
	public void test_invalid_fname_and_invalid_lname() throws Exception {
		try{
			a.Register("john","./","doe@gmail.com","12345","4");
		}
		catch(RegistrationExceptionHandler e){
			assertSame("Registration Failed", e.getMessage());
		}
	}
	
	//Test Id:25
	//Test Case: invalid type_Id and valid data enter 
	//Input: "john" and "doe" and "doe@gmail.com" and "12345" and "./"
	//Expect output: Registration Failed;
	@Test
	public void test_value_data_invalidtype_Id() throws Exception {
		try{
			a.Register("john","doe","doe@gmail.com","12345","/.");
		}
		catch(RegistrationExceptionHandler e){
			assertSame("Registration Failed", e.getMessage());
		}
	}
}