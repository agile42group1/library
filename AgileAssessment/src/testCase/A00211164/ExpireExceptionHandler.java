package testCase.A00211164;

public class ExpireExceptionHandler extends Exception {

	String message;

	public ExpireExceptionHandler(String errMessage) {
		message = errMessage;
	}

	public String getMessage() {
		return message; 
	}
} 