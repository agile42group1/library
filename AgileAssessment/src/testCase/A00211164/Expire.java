package testCase.A00211164;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Expire {

	public boolean Expire(Date date2) throws ExpireExceptionHandler, ParseException{
	
		SimpleDateFormat returnDate = new SimpleDateFormat("yyyy-MM-dd");

		Date date1 = returnDate.parse("2017-04-01");
		
		long difference = date1.getTime() - date2.getTime();
		long days = (difference)/(1000 * 60 * 60 * 24);
		long expire = days * 2;

		if (days < 0){ 
			System.out.println("Book or Journal has not yet expired");
			throw new ExpireExceptionHandler("Book or Journal not expired");
		}
		
		if (days == 0){
			System.out.println("Book or Journal Expires Today");
			throw new ExpireExceptionHandler("A book or Journal is due for return");
		}		
		
		if (days > 0){
			System.out.println("Book or Journal has expired");
		}
		return true;
		}
}