package testCase.A00211953;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.junit.Test;
import junit.framework.TestCase;
import utilise.CalculateFine;

public class CalculateFineTest extends TestCase {
	

	SimpleDateFormat returnDate = new SimpleDateFormat("yyyy-MM-dd");

	@Test
	public void test_1_29DaysOverdue() throws ParseException {

		CalculateFine testObject = new CalculateFine();

		try {
			assertTrue(testObject.Calculate(returnDate.parse("2017-03-01")));
		} catch (Exception e) {
			fail("should not come here");
		}
	}

	@Test
	public void test_2_2DaysOverdue() throws ParseException {

		CalculateFine testObject = new CalculateFine();

		try {
			assertTrue(testObject.Calculate(returnDate.parse("2017-03-29")));
		} catch (Exception e) {
			fail("should not come here");
		}
	}

	@Test
	public void test_3_1DayOverdue() throws ParseException {

		CalculateFine testObject = new CalculateFine();

		try {
			assertTrue(testObject.Calculate(returnDate.parse("2017-03-30")));
		} catch (Exception e) {
			fail("should not come here");
		}
	}

	@Test
	public void test_4_ReturnDueToday() throws ParseException {

		CalculateFine testObject = new CalculateFine();

		try {
			assertTrue(testObject.Calculate(returnDate.parse("2017-03-31")));
			fail("Should not get here .. Exception Expected");
		} catch (CalculateFineExceptionHandler e) {

			assertEquals("An edition is due for return", e.getMessage());
		}
	}

	@Test
	public void test_5_ReturnDueTomorrow() throws ParseException {

		CalculateFine testObject = new CalculateFine();

		try {
			assertTrue(testObject.Calculate(returnDate.parse("2017-04-01")));
			fail("Should not get here .. Exception Expected");
		} catch (CalculateFineExceptionHandler e) {

			assertEquals("No Fine Due", e.getMessage());
		}
	}

	@Test
	public void test_6_NoFine() throws ParseException {

		CalculateFine testObject = new CalculateFine();

		try {
			assertTrue(testObject.Calculate(returnDate.parse("2017-04-02")));
			fail("Should not get here .. Exception Expected");
		} catch (CalculateFineExceptionHandler e) {

			assertEquals("No Fine Due", e.getMessage());
		}
	}

	@Test
	public void test_7_NoFine() throws ParseException {

		CalculateFine testObject = new CalculateFine();

		try {
			assertTrue(testObject.Calculate(returnDate.parse("2017-04-03")));
			fail("Should not get here .. Exception Expected");
		} catch (CalculateFineExceptionHandler e) {

			assertEquals("No Fine Due", e.getMessage());
		}
	}

	@Test
	public void test_8_NoFine() throws ParseException {

		CalculateFine testObject = new CalculateFine();

		try {
			assertTrue(testObject.Calculate(returnDate.parse("2017-05-03")));
			fail("Should not get here .. Exception Expected");
		} catch (CalculateFineExceptionHandler e) {

			assertEquals("No Fine Due", e.getMessage());
		}
	}
}