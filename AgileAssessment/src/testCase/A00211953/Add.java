package testCase.A00211953;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import utilise.Edition;


public class Add {


	private Statement stmt = null;
	private Connection con;

	public void Input(String edition, String isbn, String title, String author,
			String publisher, String date, String category, int available,
			int total) throws AddExceptionHandler {

		if (edition == null || isbn == null || title == null || author == null
				|| publisher == null || date == null || category == null
				|| available < 1 || total < 1) {
			throw new AddExceptionHandler("Invalid entries to textfields");
		}

		else if (edition != null && isbn != null && title != null
				&& author != null && publisher != null && date != null
				&& category != null && available > 0 && total > 0) {
			throw new AddExceptionHandler("Successful");
		}
	}

	public boolean addEditionToDB(Edition newEdition) throws Exception {
		boolean result = false;
		// boolean connected=connectDB();
		boolean connected = true;
		if (connected) {
			try {
				stmt = con.createStatement();
				
				String eISBN = newEdition.geteISBN();
				String title = newEdition.geteTitle();
				String author = newEdition.getAuthor();
				String publisher = newEdition.getPublisher();
				String datePublished = newEdition.getDatePublished();
				String category = newEdition.getCategory();
				int quantityAvail = newEdition.getQunatityAvail();
				int quantityTotal = newEdition.getQuantityTotal();
				int typeId = newEdition.getTypeId();
				int userId = newEdition.getUserId();


				boolean existsISBN = checkISBN(eISBN);
				if (existsISBN) {
					throw new Exception("Duplicate ISBN");}
				
					else if( eISBN.length() != 3){
						throw new Exception("The ISBN should be a 14 digit number");}
				
				String insertStmt = "INSERT INTO edition VALUES(" + null + ",'"
						+ eISBN + "','" + title + "','" + author + "','"
						+ publisher + "','" + datePublished + "','" + category
						+ "','" + quantityAvail + "','" + quantityTotal + "','"
						+ typeId + "'," + userId + ");";
				int effectedRows = stmt.executeUpdate(insertStmt);
				if (effectedRows == 1) {
					result = true;
				}

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
					con = null;
				}
				if (stmt != null) {
					try {
						stmt.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
					stmt = null;
}	}	}
		return result;
}

	private boolean checkISBN(String eISBN) {
		// TODO Auto-generated method stub
		return false;
	}

	public void Update(String edition, String isbn, String title,
			String author, String publisher, String date, String category,
			int available, int total) throws AddExceptionHandler,
			SQLException {

		String updateStmt = "UPDATE edition SET  e_isbn ='" + isbn
				+ "', e_title ='" + title + "', e_author ='" + author
				+ "', e_publisher ='" + publisher + "', e_date_published ='"
				+ date + "', e_category ='" + category
				+ "', e_quantity_avail ='" + available
				+ "', e_quantity_total ='" + total + "'Where edition_id ='"
				+ edition + "';";
		stmt.executeUpdate(updateStmt);
		
	};

	public void Delete(String edition) throws AddExceptionHandler,
			SQLException {

		String deleteStmt = "delete from edition where edition_id = '"
				+ edition + "';";
		stmt.executeUpdate(deleteStmt);
	};
}