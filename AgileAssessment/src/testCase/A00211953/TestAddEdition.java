package testCase.A00211953;

import static org.junit.Assert.*;
import org.junit.Test;
import exception.WrongISBNFormatException;
import exception.WrongQuantityNumberException;
import utilise.Edition;
import utilise.DAO;

public class TestAddEdition {

	@Test
	public void test_empty_edition() throws Exception {
		Add testObject = new Add();
		try {
			testObject.Input(null, "", "", "", "", "", "", 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (AddExceptionHandler e) {

			assertEquals("Invalid entries to textfields", e.getMessage());
		}
	}

	@Test
	public void test_empty_isbn() throws Exception {
		Add testObject = new Add();
		try {
			testObject.Input("", null, "", "", "", "", "", 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (AddExceptionHandler e) {

			assertEquals("Invalid entries to textfields", e.getMessage());
		}
	}

	@Test
	public void test_empty_title() throws Exception {
		Add testObject = new Add();
		try {
			testObject.Input("", "", null, "", "", "", "", 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (AddExceptionHandler e) {

			assertEquals("Invalid entries to textfields", e.getMessage());
		}
	}

	@Test
	public void test_empty_author() throws Exception {
		Add testObject = new Add();
		try {
			testObject.Input("", "", "", null, "", "", "", 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (AddExceptionHandler e) {

			assertEquals("Invalid entries to textfields", e.getMessage());
		}
	}

	@Test
	public void test_empty_publisher() throws Exception {
		Add testObject = new Add();
		try {
			testObject.Input("", "", "", "", null, "", "", 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (AddExceptionHandler e) {

			assertEquals("Invalid entries to textfields", e.getMessage());
		}
	}

	@Test
	public void test_empty_date() throws Exception {
		Add testObject = new Add();
		try {
			testObject.Input("", "", "", "", "", null, "", 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (AddExceptionHandler e) {

			assertEquals("Invalid entries to textfields", e.getMessage());
		}
	}

	@Test
	public void test_empty_category() throws Exception {
		Add testObject = new Add();
		try {
			testObject.Input("", "", "", "", "", "", null, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (AddExceptionHandler e) {

			assertEquals("Invalid entries to textfields", e.getMessage());
		}
	}

	@Test
	public void test_positive_number_available() throws Exception {
		Add testObject = new Add();
		try {
			testObject.Input("", "", "", "", "", "", "", -1, 2);
			fail("Should not get here .. Exception Expected");
		} catch (AddExceptionHandler e) {

			assertEquals("Invalid entries to textfields", e.getMessage());
		}
	}

	@Test
	public void test_positive_number_total() throws Exception {
		Add testObject = new Add();
		try {
			testObject.Input("", "", "", "", "", "", "", 1, -1);
			fail("Should not get here .. Exception Expected");
		} catch (AddExceptionHandler e) {

			assertEquals("Invalid entries to textfields", e.getMessage());
		}
	}

	@Test
	public void test_all_invalid() throws Exception {
		Add testObject = new Add();
		try {
			testObject.Input(null, null, null, null, null, null, null, -1, -1);
			fail("Should not get here .. Exception Expected");
		} catch (AddExceptionHandler e) {

			assertEquals("Invalid entries to textfields", e.getMessage());
		}
	}

	@Test
	public void test_four_invalid() throws Exception {
		Add testObject = new Add();
		try {
			testObject.Input(null, "", null, null, "", null, "", 1, -1);
			fail("Should not get here .. Exception Expected");
		} catch (AddExceptionHandler e) {

			assertSame("Invalid entries to textfields", e.getMessage());
		}
	}

	@Test
	public void test_all_valid() throws Exception {
		Add testObject = new Add();
		try {
			testObject.Input("nn", "nn", "nn", "nn", "nn", "nn", "nn", 11, 11);
			fail("Should not get here .. Exception Expected");
		} catch (AddExceptionHandler e) {
			assertSame("Successful", e.getMessage());
		}
	}

	@Test
	public void DB1() {

		// create a new object and specify their attribute
		Edition addEdition = new Edition();
		addEdition.seteISBN("123456789123456");
		addEdition.seteTitle("test title");
		addEdition.setAuthor("test author");
		addEdition.setPublisher("test publisher");
		addEdition.setDatePublished("2017-11-11");
		addEdition.setCategory("test category");
		addEdition.setQunatityAvail(4);
		addEdition.setQuantityTotal(5);
		addEdition.setTypeId(1);
		addEdition.setUserId(1);

		boolean result;
		try {
			result = DAO.instance.addEditionToDB(addEdition);
			assertEquals(true, result);
		} catch (WrongISBNFormatException e) {
			e.printStackTrace();
			fail("should not come to here");
		} catch (WrongQuantityNumberException e) {
			e.printStackTrace();
			fail("should not come to here");
		}

	}

	@Test
	public void DB2() {

		// create a new object and specify their attribute
		Edition addEdition = new Edition();
		addEdition.setEditionId(1);
		addEdition.seteISBN("12345678912344");
		addEdition.seteTitle("test title");
		addEdition.setAuthor("test author");
		addEdition.setDatePublished("2017-3-29");
		addEdition.setPublisher("test publisher");
		addEdition.setCategory("test category");
		addEdition.setQunatityAvail(4);
		addEdition.setQuantityTotal(5);
		addEdition.setTypeId(2);
		addEdition.setUserId(1);

		// do the test
		try {
			boolean result = DAO.instance.addEditionToDB(addEdition);
			assertEquals(true, result);
		} catch (WrongISBNFormatException e) { // catch block
			e.printStackTrace();
			fail("should not come to here");
		} catch (WrongQuantityNumberException e) { // catch block
			e.printStackTrace();
			fail("should not come to here");
		}

	}
}
