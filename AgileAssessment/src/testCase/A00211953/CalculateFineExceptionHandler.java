package testCase.A00211953;

@SuppressWarnings("serial")
public class CalculateFineExceptionHandler  extends Exception {

String message;
	
	public CalculateFineExceptionHandler(String errMessage){
		message = errMessage;
	}
	
	public String getMessage() {
		return message; 
	}
} 
