package testCase.A00215694;





import static org.junit.Assert.*;

import java.sql.ResultSet;

import org.junit.Test;



import utilise.DAO;
import utilise.ResultTableModel;

public class SearchTest {
	//In database,there are 6 groups of data
	////////////////////////////////////////////////////////////////
// edition_id       e_title
/*  1				MySQL RDBMS
	2				A
	3				B
	4				C
	5				Time
	6				D        */
///////////////////////////////////////////////////////////////////////////	

	//TestID:1
	//input an available e_title
	// test succeed
		@Test
		public void test1()
		{
			
			
			try
			{
			
				ResultSet effectRows=(ResultSet) DAO.instance.searchFromDB("Time");
				
				String title=effectRows.getString("Title");
				assertEquals("Time",title);	
		
			}
			catch(Exception e)
			{
				fail("Not found");
			}
		}
		
		//TestID:2
		//input an available e_title
	/*	// test succeed
		@Test
		public void test2() throws Exception
		{
			
			
			try
			{				
				ResultSet effectRows=DAO.instance.searchInDB("A");
				if(effectRows.next())
				{
				String title=effectRows.getString("e_title");
				assertEquals("A",title);	
				}
				
				
			}
			catch(Exception e)
			{
				fail("Not found");
			}
		}
		//TestID:3
		//input an available e_title
		// test succeed
		@Test
		public void test3() throws Exception
		{
			
			
			try
			{
//				
				ResultSet effectRows=DAO.instance.searchInDB("MySQL RDBMS");
				if(effectRows.next())
				{
				String title=effectRows.getString("e_title");
				assertEquals("MySQL RDBMS",title);	
				}
				
				
			}
			catch(Exception e)
			{
				fail("Not found");
			}
		}
		
		//TestID:4
		//input an available e_title
		// test succeed
		@Test
		public void test4() throws Exception
		{
			
			
			try
			{
				
				ResultSet effectRows=DAO.instance.searchInDB("B");
				if(effectRows.next())
				{
				String title=effectRows.getString("e_title");
				assertEquals("B",title);	
				}
				
				
			}
			catch(Exception e)
			{
				fail("Not found");
			}
		}
		
		//TestID:5
		//input an available e_title
		// test succeed
		@Test
		public void test5() throws Exception
		{
			
			
			try
			{
//				
				ResultSet effectRows=DAO.instance.searchInDB("C");
				if(effectRows.next())
				{
				String title=effectRows.getString("e_title");
				assertEquals("C",title);	
				}
				
				
			}
			catch(Exception e)
			{
				fail("Not found");
			}
		}
		//TestID:6
		//input an available e_title
		// test succeed
		@Test
		public void test6() throws Exception
		{
			
			
			try
			{
//				
				ResultSet effectRows=DAO.instance.searchInDB("D");
				if(effectRows.next())
				{
				String name=effectRows.getString("e_title");
				assertEquals("D",name);	
				}
				
				
			}
			catch(Exception e)
			{
				fail("Not found");
			}
		}
		
		//test for edition_id
		
		//TestID:7
		//input an available edition_id
		// test succeed
		@Test
		public void test7() throws Exception
		{
			
			
			try
			{
//				
				ResultSet effectRows=DAO.instance.searchInDB("1");
				if(effectRows.next())
				{
				String id=effectRows.getString("edition_id");
				assertEquals("1",id);	
				}
				
				
			}
			catch(Exception e)
			{
				fail("Not found");
			}
		}
		//TestID:8
		//input an available edition_id
		// test succeed
		@Test
		public void test8() throws Exception
		{
			
			
			try
			{
//				
				ResultSet effectRows=DAO.instance.searchInDB("2");
				if(effectRows.next())
				{
				String id=effectRows.getString("edition_id");
				assertEquals("2",id);	
				}
				
				
			}
			catch(Exception e)
			{
				fail("Not found");
			}
		}
		
		
		//TestID:9
		//input an available edition_id
		// test succeed
		@Test
		public void test9() throws Exception
		{
			
			
			try
			{
//				
				ResultSet effectRows=DAO.instance.searchInDB("3");
				if(effectRows.next())
				{
				String id=effectRows.getString("edition_id");
				assertEquals("3",id);	
				}
				
				
			}
			catch(Exception e)
			{
				fail("Not found");
			}
		}
		//TestID:10
		//input an available edition_id
		// test succeed
		@Test
		public void test10() throws Exception
		{
			
			
			try
			{
			
				ResultSet effectRows=DAO.instance.searchInDB("4");
				if(effectRows.next())
				{
					String id=effectRows.getString("edition_id");
					assertEquals("4",id);	
	
				}
				

			}
			catch(Exception e)
			{
				fail("Not found");
			}
		}
		//TestID:11
		//input an available edition_id
		// test succeed
		@Test
		public void test11() throws Exception
		{
			
			
			try
			{
			
				ResultSet effectRows=DAO.instance.searchInDB("5");
				if(effectRows.next())
				{
					String id=effectRows.getString("edition_id");
					assertEquals("5",id);	
	
				}
				
			}
			catch(Exception e)
			{
				fail("Not found");
			}
		}
		//TestID:12
		//input an available edition_id
		// test succeed
		@Test
		public void test12() throws Exception
		{
			
			
			try
			{
			
				ResultSet effectRows=DAO.instance.searchInDB("6");
				if(effectRows.next())
				{
					String id=effectRows.getString("edition_id");
					assertEquals("6",id);	
	
				}
			}
			catch(Exception e)
			{
				fail("Not found");
			}
		}
		*/
		
}
