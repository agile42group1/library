package gui;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Point;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;

import utilise.DAO;
import utilise.LibraryMS;
import utilise.Administrator;
import utilise.ResultTableModel;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;




import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.SwingConstants;

public class EditionInterface {

	public JFrame frame;
	private JTable table;
	private JScrollPane scrollPane;
	private ResultTableModel tableModel;
	private Administrator currentAdmin;
	private JButton btnSearch;
	private JLabel lblIsbnOrTitle;
	private JTextField textField;
	private JButton btnBackToMain;
	private JLabel lblEdition;
	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					EditionInterface window = new EditionInterface();
//					window.frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the application.
	 */
	public EditionInterface(Administrator admin) {
		tableModel=new ResultTableModel();
		initialize();
		currentAdmin=admin;
		DAO.instance.refreshTableModel(tableModel);
		tableModel.fireTableChanged(null);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(153, 204, 204));
		frame.setBounds(100, 100, 730, 540);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		table = new JTable(tableModel);
		table.setPreferredScrollableViewportSize(new Dimension(200,200));
		scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setMaximumSize(new Dimension(100, 100));
		scrollPane.setLocation(new Point(100, 100));
		scrollPane
				.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		scrollPane.setBounds(20, 145, 543, 340);

		scrollPane.setVisible(true);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(scrollPane);
		
		btnSearch = new JButton("Search");
		btnSearch.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String key=textField.getText();
				if(key.length()!=0)
				{
				tableModel=currentAdmin.search(key);
				table.setModel(tableModel);
				tableModel.fireTableChanged(null);
				}
				else
				{
					DAO.instance.refreshTableModel(tableModel);
				}
			}
		});
		btnSearch.setBounds(429, 85, 131, 38);
		frame.getContentPane().add(btnSearch);
		
		lblIsbnOrTitle = new JLabel("ISBN or Title:");
		lblIsbnOrTitle.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblIsbnOrTitle.setBounds(20, 85, 134, 38);
		frame.getContentPane().add(lblIsbnOrTitle);
		
		textField = new JTextField();
		textField.setBounds(135, 87, 234, 34);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnAddNew = new JButton("Add New");
		btnAddNew.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnAddNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpdateInterface ui=new UpdateInterface(currentAdmin,"Insert",0);
				ui.frmCollegeLibraryAdministrator.setVisible(true);
			}
		});
		btnAddNew.setBounds(573, 163, 131, 38);
		frame.getContentPane().add(btnAddNew);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int[] selectedRows = table.getSelectedRows();
				if(selectedRows.length==0)
				{
					new JOptionPane().showMessageDialog(null, "You don't select any entry");
				}
				else if(selectedRows.length>1)
				{
					new JOptionPane().showMessageDialog(null, "You could only select one row");
				}
				else
				{
					int editionId=Integer.parseInt((String) table.getValueAt(selectedRows[0], 0));
					UpdateInterface ui=new UpdateInterface(currentAdmin,"Update",editionId);
					ui.frmCollegeLibraryAdministrator.setVisible(true);
				}
			}
		});
		btnUpdate.setBounds(573, 237, 131, 38);
		frame.getContentPane().add(btnUpdate);
		
		btnBackToMain = new JButton("Back To Main");
		btnBackToMain.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnBackToMain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//AdministratorHome AdministratorHome = new AdministratorHome(currentAdmin);
				//AdministratorHome.frame.setVisible(true);
				frame.dispose();
			}
		});
		btnBackToMain.setBounds(573, 447, 131, 38);
		frame.getContentPane().add(btnBackToMain);
		
		lblEdition = new JLabel("View Edition");
		lblEdition.setHorizontalAlignment(SwingConstants.CENTER);
		lblEdition.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblEdition.setBounds(10, 25, 694, 34);
		frame.getContentPane().add(lblEdition);
		
		JButton btnReturn = new JButton("Return");
		btnReturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int[] selectedRows = table.getSelectedRows();
				if(selectedRows.length==0)
				{
					new JOptionPane().showMessageDialog(null, "You don't select any entry");
				}
				else if(selectedRows.length>1)
				{
					new JOptionPane().showMessageDialog(null, "You could only select one row");
				}
				else
				{
					int editionId=Integer.parseInt((String) table.getValueAt(selectedRows[0], 0));
					int currentAvaiEdiNo=Integer.parseInt((String)table.getValueAt(selectedRows[0],7));
					int currentTotalEdiNo=Integer.parseInt((String)table.getValueAt(selectedRows[0],8));
					if(currentAvaiEdiNo<currentTotalEdiNo)
					{
						currentAdmin.returnEdition(editionId);	
						DAO.instance.refreshTableModel(tableModel);
						if(currentAvaiEdiNo==0)
						{
							//send the notiification to all users.
							String isbn=(String)table.getValueAt(selectedRows[0], 1);
							String title=(String)table.getValueAt(selectedRows[0], 2);
							new LibraryMS().notifyAvailableEditions(isbn, title);
						}
					}
					else
					{
						new JOptionPane().showMessageDialog(null, "No borrow to this book, don't need to return");
					}
				}
			}
		});
		btnReturn.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnReturn.setBounds(573, 300, 131, 38);
		frame.getContentPane().add(btnReturn);
	}
}
