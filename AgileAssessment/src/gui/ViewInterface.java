package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JList;

import utilise.Student;

import javax.swing.JLabel;


import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.SwingConstants;

public class ViewInterface {

	public JFrame frame;
	private Student currentStudent;


	/**
	 * Create the application.
	 */
	public ViewInterface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(153, 204, 204));
		frame.setBounds(100, 100, 730, 540);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JList list = new JList();
		list.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		list.setBounds(69, 135, 564, 270);
		frame.getContentPane().add(list);
		
		JLabel lblRequestLists = new JLabel("Request lists:");
		lblRequestLists.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblRequestLists.setBounds(69, 99, 108, 25);
		frame.getContentPane().add(lblRequestLists);
		
		JButton btnNewButton = new JButton("Back To Main");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//StudentHome StudentHome = new StudentHome(currentStudent);
				//StudentHome.frame.setVisible(true);
				frame.dispose();
			}
		});
		btnNewButton.setBounds(69, 427, 144, 45);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblViewRequests = new JLabel("View Requests");
		lblViewRequests.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblViewRequests.setHorizontalAlignment(SwingConstants.CENTER);
		lblViewRequests.setBounds(10, 31, 694, 40);
		frame.getContentPane().add(lblViewRequests);
	}
}
