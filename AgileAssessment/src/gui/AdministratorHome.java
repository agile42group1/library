package gui;

import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import utilise.Administrator;
import utilise.LibraryMS;
import utilise.ResultTableModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;

public class AdministratorHome {

	public JFrame frame;
	private Administrator currentAdmin;
	
	private ResultTableModel tableModel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdministratorHome window = new AdministratorHome(null);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AdministratorHome(Administrator admin) {
		currentAdmin=admin;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		tableModel = new ResultTableModel();
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(153, 204, 204));
		frame.setBounds(100, 100, 730, 540);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblWelcomeAdministratorName = new JLabel("Welcome! "+currentAdmin.getFullName());
		lblWelcomeAdministratorName.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblWelcomeAdministratorName.setBounds(271, 128, 406, 86);
		frame.getContentPane().add(lblWelcomeAdministratorName);
		
		JButton btnViewRequest = new JButton("View Request");
		btnViewRequest.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnViewRequest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					tableModel = currentAdmin.viewRequest();
					RequestInterface window = new RequestInterface(currentAdmin,tableModel);
					window.frame.show();
				}catch(Exception e){
					e.printStackTrace();
				}
					
			
			}
		});
		btnViewRequest.setBounds(72, 137, 149, 51);
		frame.getContentPane().add(btnViewRequest);
		
		JButton btnEdition = new JButton("Edition");
		btnEdition.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnEdition.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EditionInterface window = new EditionInterface(currentAdmin);
				window.frame.setVisible(true);
			}
		});
		btnEdition.setBounds(72, 209, 149, 44);
		frame.getContentPane().add(btnEdition);
		
		JButton btnLogOff = new JButton("Log off");
		btnLogOff.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnLogOff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Authentication Authentication = new Authentication();
				Authentication.frame.setVisible(true);
				frame.dispose();
			}
		});
		btnLogOff.setBounds(72, 342, 149, 51);
		frame.getContentPane().add(btnLogOff);
		
		JLabel lblCollegeLibraryAdministrator = new JLabel("College Library Administrator Interface");
		lblCollegeLibraryAdministrator.setHorizontalAlignment(SwingConstants.CENTER);
		lblCollegeLibraryAdministrator.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblCollegeLibraryAdministrator.setBounds(10, 26, 694, 65);
		frame.getContentPane().add(lblCollegeLibraryAdministrator);
		
		JButton btnNotifyInterRequest = new JButton("Inter-request");
		btnNotifyInterRequest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new LibraryMS().notifyInterlib();
			}
		});
		btnNotifyInterRequest.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNotifyInterRequest.setBounds(72, 279, 149, 44);
		frame.getContentPane().add(btnNotifyInterRequest);
	}
}
