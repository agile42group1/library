package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JLabel;
import javax.swing.JTextPane;

import utilise.Student;

import java.awt.Font;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class NotificationInterface {

	private JFrame frame;
	private Student currentStudent;

	/**
	 * Create the application.
	 */
	public NotificationInterface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(153, 204, 204));
		frame.setBounds(100, 100, 730, 540);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JList list = new JList();
		list.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		list.setBounds(39, 137, 308, 282);
		frame.getContentPane().add(list);
		
		JLabel lblNotificationLists = new JLabel("Notification Lists:");
		lblNotificationLists.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNotificationLists.setBounds(39, 90, 133, 36);
		frame.getContentPane().add(lblNotificationLists);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(381, 137, 308, 282);
		frame.getContentPane().add(textPane);
		
		JLabel lblNotification = new JLabel("Notification:");
		lblNotification.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNotification.setBounds(381, 93, 116, 30);
		frame.getContentPane().add(lblNotification);
		
		JButton btnNewButton = new JButton("Back To Main");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.setBounds(39, 435, 149, 41);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblViewNotifications = new JLabel("View Notifications");
		lblViewNotifications.setHorizontalAlignment(SwingConstants.CENTER);
		lblViewNotifications.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblViewNotifications.setBounds(10, 21, 694, 41);
		frame.getContentPane().add(lblViewNotifications);
	}
}
