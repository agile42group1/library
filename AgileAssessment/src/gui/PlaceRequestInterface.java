package gui;

import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;

import exception.UnavailableRequestException;
import exception.WrongRequestException;
import utilise.Student;

import javax.swing.JButton;
import javax.swing.JLabel;

import utilise.DAO;
import utilise.ResultTableModel;

import java.awt.event.ActionListener;
import java.text.ParseException;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.SwingConstants;

/**
 * Please referee the word document by Michael and the user story which code is
 * PLR6
 */

public class PlaceRequestInterface {


	public JFrame frame;

	// 3 object used to display the query result of search
	private JTable table;
	private ResultTableModel tableModel;
	private JScrollPane scrollPane;
	private Student stu;
	private JButton btnBackToMain;
	private JLabel lblPlaceRequest;
	private String option;

	/**
	 * Create the application.
	 */
	public PlaceRequestInterface(Student stu, ResultTableModel tm,String option) {
		this.option=option;
		this.stu = stu;
		this.tableModel = tm;
		initialize();
		tableModel.fireTableChanged(null);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(153, 204, 204));
		frame.setBounds(100, 100, 720, 530);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		table = new JTable(tableModel);
		table.setPreferredScrollableViewportSize(new Dimension(900, 300));
		scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane
				.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		scrollPane.setBounds(56, 117, 614, 274);

		scrollPane.setVisible(true);
		frame.getContentPane().add(scrollPane);

		JButton btnPlaceRequest = new JButton("Place Request");
		btnPlaceRequest.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnPlaceRequest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				// get all selected rows
				int[] selectedRows = table.getSelectedRows();
				int[] selectedEditions = new int[selectedRows.length];
				for (int i = 0; i < selectedRows.length; i++) {
					Integer tempId = Integer.parseInt((String) table.getValueAt(selectedRows[i], 0));
					selectedEditions[i] = tempId;
				}

				// place request to selected rows
				// assume the current student is 1
				try {
					if(option.equals("local library"))
					{
						stu.placeRequest(selectedEditions);
						DAO.instance.refreshTableModel(tableModel);
						new JOptionPane().showMessageDialog(null, "Your request has been processed sucessfully");
					}
					else if(option.equals("inter library"))
					{
						
							Integer tempId = Integer.parseInt((String) table.getValueAt(selectedRows[0], 0));
							String isbn=(String)table.getValueAt(selectedRows[0], 1);
							stu.placeInterlibraryRequest(tempId,isbn);
							DAO.instance.refreshInterEdition(tableModel);
					}

				} catch (WrongRequestException e) {
					// TODO Auto-generated catch block
					new JOptionPane().showMessageDialog(null, "Your request edition doesn't exists or invlid user");
				} catch (UnavailableRequestException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
					new JOptionPane().showMessageDialog(null, "Your request contains unavailable editon");
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					new JOptionPane().showMessageDialog(null, "invalid date");
				}
			}
		});
		btnPlaceRequest.setBounds(295, 413, 159, 47);
		frame.getContentPane().add(btnPlaceRequest);

		JLabel lblSearchResults = new JLabel("Search Results:");
		lblSearchResults.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblSearchResults.setBounds(56, 73, 149, 33);
		frame.getContentPane().add(lblSearchResults);

		JButton btnNotifyMe = new JButton("Notify me");
		btnNotifyMe.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNotifyMe.setBounds(56, 413, 159, 47);
		frame.getContentPane().add(btnNotifyMe);
		
		btnBackToMain = new JButton("Back To Main");
		btnBackToMain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		btnBackToMain.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnBackToMain.setBounds(511, 413, 159, 47);
		frame.getContentPane().add(btnBackToMain);
		
		lblPlaceRequest = new JLabel("Place Request");
		lblPlaceRequest.setHorizontalAlignment(SwingConstants.CENTER);
		lblPlaceRequest.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblPlaceRequest.setBounds(10, 26, 684, 39);
		frame.getContentPane().add(lblPlaceRequest);
	}
}
