package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import utilise.ResultTableModel;
import utilise.Student;

import javax.swing.JList;

import java.awt.Font;
import java.awt.Point;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.border.LineBorder;

import testCase.A00211953.CalculateFineExceptionHandler;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * Please referee the word document by Michael and the user story which code is
 * PLR6
 */

public class CalculateFine {

	public JFrame frame;
	private JTextField issueField;
	private JTextField expiryField;
	private JTextField fineField;
	private Student currentStudent;
	private ResultTableModel rtm;
	private JTable table;

	/**
	 * Create the application.
	 */
	public CalculateFine(Student cs) {
		currentStudent = cs;
		rtm = currentStudent.readOverDueLoan();
		initialize();

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(153, 204, 204));
		frame.setBounds(100, 100, 730, 540);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblIssueDate = new JLabel("Issue Date:");
		lblIssueDate.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblIssueDate.setBounds(365, 109, 108, 32);
		frame.getContentPane().add(lblIssueDate);

		issueField = new JTextField();
		issueField.setEditable(false);
		issueField.setBounds(483, 112, 185, 30);
		frame.getContentPane().add(issueField);
		issueField.setColumns(10);

		JLabel lblExpire = new JLabel("Expiry Date:");
		lblExpire.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblExpire.setBounds(365, 187, 108, 32);
		frame.getContentPane().add(lblExpire);

		expiryField = new JTextField();
		expiryField.setEditable(false);
		expiryField.setBounds(483, 190, 185, 30);
		frame.getContentPane().add(expiryField);
		expiryField.setColumns(10);

		JLabel lblFineLists = new JLabel("Over Due Loan");
		lblFineLists.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblFineLists.setBounds(31, 87, 122, 21);
		frame.getContentPane().add(lblFineLists);

		JLabel lblFine = new JLabel("Fine:");
		lblFine.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblFine.setBounds(365, 259, 108, 32);
		frame.getContentPane().add(lblFine);

		fineField = new JTextField();
		fineField.setEditable(false);
		fineField.setBounds(483, 262, 185, 30);
		frame.getContentPane().add(fineField);
		fineField.setColumns(10);

		JButton btnNewButton = new JButton("Back To Main");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// StudentHome StudentHome = new StudentHome(currentStudent);
				// StudentHome.frame.setVisible(true);
				frame.dispose();
			}
		});
		btnNewButton.setBounds(365, 426, 157, 41);
		frame.getContentPane().add(btnNewButton);

		JButton btnCalculateFine = new JButton("Calculate Fine");
		btnCalculateFine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				utilise.CalculateFine cf=new utilise.CalculateFine();
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				try {
					Date date=sdf.parse(expiryField.getText());
					cf.Calculate(date);
					long fine=cf.getFine();
					fineField.setText(""+fine);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (CalculateFineExceptionHandler e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		btnCalculateFine.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnCalculateFine.setBounds(365, 342, 157, 41);
		frame.getContentPane().add(btnCalculateFine);

		JLabel lblCalculateFine = new JLabel("Calculate Fine");
		lblCalculateFine.setHorizontalAlignment(SwingConstants.CENTER);
		lblCalculateFine.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblCalculateFine.setBounds(10, 22, 694, 41);
		frame.getContentPane().add(lblCalculateFine);

		table = new JTable(rtm);
		table.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				int selectedRow=table.getSelectedRow();
				String issueDate=(String)table.getValueAt(selectedRow, 1);
				String expiryDate=(String)table.getValueAt(selectedRow, 2);
				issueField.setText(issueDate);
				expiryField.setText(expiryDate);
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

		});

		JScrollPane scrollPane = new JScrollPane();

		scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setMaximumSize(new Dimension(100, 100));
		scrollPane.setLocation(new Point(100, 100));
		scrollPane.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		scrollPane.setBounds(41, 109, 251, 308);
		scrollPane.setVisible(true);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(scrollPane);
	}
}
