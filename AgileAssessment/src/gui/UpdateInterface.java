package gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import exception.DuplicateISBNException;
import exception.WrongISBNFormatException;
import exception.WrongQuantityNumberException;
import utilise.Administrator;
import utilise.Edition;
import java.awt.Color;

public class UpdateInterface {



	public JFrame frmCollegeLibraryAdministrator;
	private JTextField textFieldIsbn;
	private JTextField textFieldTitle;
	private JTextField textFieldAuthor;
	private JTextField textFieldPublisher;
	private JTextField textFieldDatePublished;
	private JTextField textFieldCategory;
	private JTextField textFieldlQuantityAvailable;
	private JTextField textFieldQuantityTotal;
	private String option;
	private Administrator currentAdmin;
	private int editionId;



	/**
	 * Create the application.
	 */
	public UpdateInterface(utilise.Administrator currentAdmin2,String op,int editionId) {
		currentAdmin=currentAdmin2;
		option=op;
		this.editionId=editionId;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCollegeLibraryAdministrator = new JFrame();
		frmCollegeLibraryAdministrator.getContentPane().setBackground(new Color(153, 204, 204));
		frmCollegeLibraryAdministrator
				.setTitle("B1 College Library Management System");
		frmCollegeLibraryAdministrator.setBounds(20, 20, 730, 540);
		frmCollegeLibraryAdministrator
				.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmCollegeLibraryAdministrator.getContentPane().setLayout(null);

		JLabel lblIsbn = new JLabel("ISBN:");
		lblIsbn.setBounds(102, 100, 193, 20);
		lblIsbn.setHorizontalAlignment(SwingConstants.RIGHT);
		lblIsbn.setVerticalAlignment(SwingConstants.BOTTOM);
		lblIsbn.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmCollegeLibraryAdministrator.getContentPane().add(lblIsbn);

		JLabel lblTitle = new JLabel("Title:");
		lblTitle.setBounds(102, 133, 193, 20);
		lblTitle.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTitle.setVerticalAlignment(SwingConstants.BOTTOM);
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmCollegeLibraryAdministrator.getContentPane().add(lblTitle);

		JLabel lblAuthor = new JLabel("Author:");
		lblAuthor.setBounds(102, 166, 193, 20);
		lblAuthor.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAuthor.setVerticalAlignment(SwingConstants.BOTTOM);
		lblAuthor.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmCollegeLibraryAdministrator.getContentPane().add(lblAuthor);

		JLabel lblPublisher = new JLabel("Publisher:");
		lblPublisher.setBounds(102, 199, 193, 20);
		lblPublisher.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPublisher.setVerticalAlignment(SwingConstants.BOTTOM);
		lblPublisher.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmCollegeLibraryAdministrator.getContentPane().add(lblPublisher);

		JLabel lblDatePublished = new JLabel("Date Published yyyy-mm-dd:");
		lblDatePublished.setBounds(102, 232, 193, 20);
		lblDatePublished.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDatePublished.setVerticalAlignment(SwingConstants.BOTTOM);
		lblDatePublished.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmCollegeLibraryAdministrator.getContentPane().add(lblDatePublished);

		JLabel lblCategory = new JLabel("Category:");
		lblCategory.setBounds(102, 265, 193, 20);
		lblCategory.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCategory.setVerticalAlignment(SwingConstants.BOTTOM);
		lblCategory.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmCollegeLibraryAdministrator.getContentPane().add(lblCategory);

		JLabel lblQuantityAvailable = new JLabel("Quantity available:");
		lblQuantityAvailable.setBounds(102, 298, 193, 20);
		lblQuantityAvailable.setHorizontalAlignment(SwingConstants.RIGHT);
		lblQuantityAvailable.setVerticalAlignment(SwingConstants.BOTTOM);
		lblQuantityAvailable.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmCollegeLibraryAdministrator.getContentPane().add(
				lblQuantityAvailable);

		JLabel lblQuantityTotal = new JLabel("Quantity total:");
		lblQuantityTotal.setBounds(102, 329, 193, 20);
		lblQuantityTotal.setHorizontalAlignment(SwingConstants.RIGHT);
		lblQuantityTotal.setVerticalAlignment(SwingConstants.BOTTOM);
		lblQuantityTotal.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmCollegeLibraryAdministrator.getContentPane().add(lblQuantityTotal);

		JLabel lblType = new JLabel("Type:");
		lblType.setBounds(102, 362, 193, 20);
		lblType.setHorizontalAlignment(SwingConstants.RIGHT);
		lblType.setVerticalAlignment(SwingConstants.BOTTOM);
		lblType.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmCollegeLibraryAdministrator.getContentPane().add(lblType);

		JLabel lblRegisteredBy = new JLabel("Registered by:");
		lblRegisteredBy.setBounds(102, 393, 193, 20);
		lblRegisteredBy.setHorizontalAlignment(SwingConstants.RIGHT);
		lblRegisteredBy.setVerticalAlignment(SwingConstants.BOTTOM);
		lblRegisteredBy.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmCollegeLibraryAdministrator.getContentPane().add(lblRegisteredBy);

		textFieldIsbn = new JTextField();
		textFieldIsbn.setBounds(336, 100, 200, 20);
		textFieldIsbn.setColumns(10);
		frmCollegeLibraryAdministrator.getContentPane().add(textFieldIsbn);

		textFieldTitle = new JTextField();
		textFieldTitle.setBounds(336, 133, 200, 20);
		textFieldTitle.setColumns(10);
		frmCollegeLibraryAdministrator.getContentPane().add(textFieldTitle);

		textFieldAuthor = new JTextField();
		textFieldAuthor.setBounds(336, 166, 200, 20);
		textFieldAuthor.setColumns(10);
		frmCollegeLibraryAdministrator.getContentPane().add(textFieldAuthor);

		textFieldPublisher = new JTextField();
		textFieldPublisher.setBounds(336, 199, 200, 20);
		textFieldPublisher.setColumns(10);
		frmCollegeLibraryAdministrator.getContentPane().add(textFieldPublisher);

		textFieldDatePublished = new JTextField();
		textFieldDatePublished.setBounds(336, 232, 200, 20);
		textFieldDatePublished.setColumns(10);
		frmCollegeLibraryAdministrator.getContentPane().add(
				textFieldDatePublished);

		textFieldCategory = new JTextField();
		textFieldCategory.setBounds(336, 265, 200, 20);
		textFieldCategory.setColumns(10);
		frmCollegeLibraryAdministrator.getContentPane().add(textFieldCategory);

		textFieldlQuantityAvailable = new JTextField();
		textFieldlQuantityAvailable.setBounds(336, 298, 200, 20);
		textFieldlQuantityAvailable.setColumns(10);
		frmCollegeLibraryAdministrator.getContentPane().add(
				textFieldlQuantityAvailable);

		textFieldQuantityTotal = new JTextField();
		textFieldQuantityTotal.setBounds(336, 329, 200, 20);
		textFieldQuantityTotal.setColumns(10);
		frmCollegeLibraryAdministrator.getContentPane().add(
				textFieldQuantityTotal);

		String[] TypeStrings = { "Select", "Book", "Journal" };
		final JComboBox comboBoxType = new JComboBox(TypeStrings);
		comboBoxType.setBounds(336, 362, 200, 20);
		comboBoxType.setToolTipText("");
		frmCollegeLibraryAdministrator.getContentPane().add(comboBoxType);

		String[] AdminStrings = { "Select", "Sergejs Trubacovs", "Shane Walsh",
				"Brian Mahon", "Dingcheng Lu", "Mirza Munawar", "Xusheng Wu",
				"Zhilin Zhang" };
		final JComboBox comboBoxRegisteredBy = new JComboBox(AdminStrings);
		comboBoxRegisteredBy.setBounds(336, 393, 200, 20);
		frmCollegeLibraryAdministrator.getContentPane().add(
				comboBoxRegisteredBy);
		
		JButton btnNewButton = new JButton(option);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Edition updatedEdition=new Edition();
				updatedEdition.setEditionId(editionId);
				String isbn=textFieldIsbn.getText();
				updatedEdition.seteISBN(isbn);
				String title=textFieldTitle.getText();
				updatedEdition.seteTitle(title);
				String author=textFieldAuthor.getText();
				updatedEdition.setAuthor(author);
				String publisher=textFieldPublisher.getText();
				updatedEdition.setPublisher(publisher);
				String date=textFieldDatePublished.getText();
				updatedEdition.setDatePublished(date);
				String category=textFieldCategory.getText();
				updatedEdition.setCategory(category);
				int qunatiAvai=Integer.parseInt(textFieldlQuantityAvailable.getText());
				updatedEdition.setQunatityAvail(qunatiAvai);
				int quantityTotal=Integer.parseInt(textFieldQuantityTotal.getText());
				updatedEdition.setQuantityTotal(quantityTotal);
				
				String type=(String)comboBoxType.getSelectedItem();
				int typeId=-1;
				if(type.equals("Book"))
				{
					typeId=1;
				}
				else if(type.equals("Journal"))
				{
					typeId=2;
				}
	
					try {
						if(option.equals("Insert"))
						{
							updatedEdition.setTypeId(typeId);
							updatedEdition.setUserId(1);
							currentAdmin.addEdition(updatedEdition);
							new JOptionPane().showMessageDialog(null, "Insert Sucessfully");
						}
						else
						{
							currentAdmin.updateEdition(updatedEdition);
							new JOptionPane().showMessageDialog(null, "Update Sucessfully");
						}
					} catch (DuplicateISBNException e1) {
						// TODO Auto-generated catch block
						new JOptionPane().showMessageDialog(null, "Duplicate ISBN");
					} catch (WrongISBNFormatException e1) {
						// TODO Auto-generated catch block
						new JOptionPane().showMessageDialog(null, "Wrong ISBN format, the ISBN should be 14 length number array");
					} catch (WrongQuantityNumberException e1) {
						// TODO Auto-generated catch block
						new JOptionPane().showMessageDialog(null, "Wrong quantity number");
					}
				
			}
		});
		btnNewButton.setBounds(160, 439, 135, 38);
		frmCollegeLibraryAdministrator.getContentPane().add(btnNewButton);
		
		JLabel lblUpdateBookOr = new JLabel("Update Book or Journal");
		lblUpdateBookOr.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblUpdateBookOr.setHorizontalAlignment(SwingConstants.CENTER);
		lblUpdateBookOr.setBounds(10, 25, 684, 38);
		frmCollegeLibraryAdministrator.getContentPane().add(lblUpdateBookOr);
		
		JButton btnBackTo = new JButton("Back To Edition");
		btnBackTo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

			}
		});
		btnBackTo.setBounds(319, 439, 135, 38);
		frmCollegeLibraryAdministrator.getContentPane().add(btnBackTo);
	}
}
