package gui;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.Color;

public class Home {

	public JFrame frame;

	/**
	 * Launch the application.
	 */


	/**
	 * Create the application.
	 */
	public Home() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(153, 204, 204));
		frame.setBounds(100, 100, 600, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblWelcome = new JLabel("Welcome To College Library");
		lblWelcome.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcome.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblWelcome.setBounds(10, 21, 564, 57);
		frame.getContentPane().add(lblWelcome);

		JButton btnLogin = new JButton("Login");
		btnLogin.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Authentication().frame.setVisible(true);
				frame.dispose();
			}
		});
		btnLogin.setBounds(88, 207, 134, 45);
		frame.getContentPane().add(btnLogin);

		JButton btnRegistration = new JButton("Registration");
		btnRegistration.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnRegistration.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RegisterGUI register=new RegisterGUI();
				register.frame.setVisible(true);
				frame.dispose();
			}
		});
		btnRegistration.setBounds(354, 207, 134, 45);
		frame.getContentPane().add(btnRegistration);

		JButton btnExit = new JButton("Exit");
		btnExit.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnExit.setBounds(222, 301, 131, 45);
		frame.getContentPane().add(btnExit);
		
		JLabel lblLoginOrRegister = new JLabel("Login Or Register:");
		lblLoginOrRegister.setHorizontalAlignment(SwingConstants.CENTER);
		lblLoginOrRegister.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblLoginOrRegister.setBounds(20, 89, 554, 57);
		frame.getContentPane().add(lblLoginOrRegister);

		RegisterGUI RegisterGUI = new RegisterGUI();
		Authentication Authentication = new Authentication();
	}
}
