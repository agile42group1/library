package gui;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;

import utilise.Student;

import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;


/**
 * Please referee the word document by Michael and the user story which code is PLR6
 * */




public class StudentHome {

	public JFrame frame;
	private SearchInterface searchInterface;
	private CalculateFine calculateInterface;
	private ViewInterface viewInterface;
	private Student currentStudent;
	
	/**
	 * Launch the application.
	 *
	 *
	/**
	 * Create the application.
	 */
	public StudentHome(Student stu) {
		this.currentStudent=stu;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(153, 204, 204));
		frame.setBounds(100, 100, 730, 540);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				searchInterface.frame.setVisible(true);
				//frame.dispose();
			}
		});
		btnSearch.setBounds(81, 99, 152, 52);
		frame.getContentPane().add(btnSearch);
		
		JButton btnLogOff = new JButton("Log Off");
		btnLogOff.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnLogOff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Authentication Authentication = new Authentication();
				Authentication.frame.setVisible(true);
				frame.dispose();
			}
		});
		btnLogOff.setBounds(81, 275, 152, 52);
		frame.getContentPane().add(btnLogOff);
		
		JButton btnCalculateFine = new JButton("Calculate Fine");
		btnCalculateFine.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnCalculateFine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			calculateInterface.frame.setVisible(true);
			//frame.dispose();
			}
		});
		btnCalculateFine.setBounds(81, 183, 152, 52);
		frame.getContentPane().add(btnCalculateFine);
		
		JLabel lblWelcome = new JLabel("Welcome !"+currentStudent.getFullName());
		lblWelcome.setFont(new Font("Dialog", Font.PLAIN, 22));
		lblWelcome.setBounds(290, 173, 414, 67);
		frame.getContentPane().add(lblWelcome);
		
		JLabel lblCollegeLibraryStudent = new JLabel("College Library Student Interface");
		lblCollegeLibraryStudent.setHorizontalAlignment(SwingConstants.CENTER);
		lblCollegeLibraryStudent.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblCollegeLibraryStudent.setBounds(10, 11, 694, 67);
		frame.getContentPane().add(lblCollegeLibraryStudent);
		searchInterface=new SearchInterface(currentStudent);
		calculateInterface=new CalculateFine(currentStudent);
		viewInterface=new ViewInterface();
	}
}
