package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;

import utilise.ResultTableModel;
import utilise.Student;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;

/**
 * Please referee the word document by Michael and the user story which code is
 * PLR6
 */

public class SearchInterface {

	public JFrame frame;
	private PlaceRequestInterface placeInterface;
	private JTextField textField;
	private ResultTableModel tableModel;

	private utilise.Student stu;
	private JButton btnBackToMain;
	private JLabel lblSearch;

	/**
	 * Create the application.
	 */
	public SearchInterface(utilise.Student currentStudent) {
		this.stu = currentStudent;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		tableModel = new ResultTableModel();
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(153, 204, 204));
		frame.setBounds(100, 100, 730, 540);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		textField = new JTextField();
		textField.setBounds(358, 118, 211, 38);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		JLabel lblNewLabel = new JLabel("Keywords to search:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(164, 116, 158, 38);
		frame.getContentPane().add(lblNewLabel);

		JButton btnNewButton = new JButton("Search by Id or Title");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String key=textField.getText();
					tableModel = stu.search(key);
					PlaceRequestInterface placeInterface1 = new PlaceRequestInterface(stu, tableModel,"local library");
					placeInterface1.frame.show();
					// frame.dispose();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(260, 203, 211, 49);
		frame.getContentPane().add(btnNewButton);
		
		btnBackToMain = new JButton("Back To Main");
		btnBackToMain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//StudentHome StudentHome = new StudentHome(stu);
				//StudentHome.frame.setVisible(true);
				frame.dispose();
			}
		});
		btnBackToMain.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnBackToMain.setBounds(260, 411, 211, 49);
		frame.getContentPane().add(btnBackToMain);
		
		lblSearch = new JLabel("Search");
		lblSearch.setHorizontalAlignment(SwingConstants.CENTER);
		lblSearch.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblSearch.setBounds(10, 33, 694, 38);
		frame.getContentPane().add(lblSearch);
		
		JButton btnInterlibrarySearch = new JButton("Interlibrary Search");
		btnInterlibrarySearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String key=textField.getText();
					tableModel = stu.searchInterlibraryRequest(key);
					PlaceRequestInterface placeInterface1 = new PlaceRequestInterface(stu, tableModel,"inter library");
					placeInterface1.frame.show();
					// frame.dispose();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnInterlibrarySearch.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnInterlibrarySearch.setBounds(260, 308, 211, 49);
		frame.getContentPane().add(btnInterlibrarySearch);
		// placeInterface=new PlaceRequestInterface();
	}
}
