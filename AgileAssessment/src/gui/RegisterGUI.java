package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.*;

import javax.swing.*;
import javax.swing.table.*;

import com.mysql.jdbc.PreparedStatement;

import utilise.Student;

import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class RegisterGUI {

	//private static final String Onclick = null;
	// JDBC driver name and database URL
	//static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	//static final String DB_URL = "jdbc:mysql://localhost:3306/collegeLibraryDB";
	// DB Connectivity Attributes

	public  JFrame frame;
	private JTextField fname;
	private JTextField lname;
	private JTextField email;
	private JTextField password;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegisterGUI window = new RegisterGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		});
	}

	/**
	 * Create the application.
	 */
	public RegisterGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		Connection conn = null;
		Statement stmt = null;

		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(153, 204, 204));
		frame.setBounds(100, 100, 600, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("Name:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(126, 79, 56, 29);
		frame.getContentPane().add(lblNewLabel);

		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEmail.setBounds(126, 172, 56, 29);
		frame.getContentPane().add(lblEmail);

		JLabel lblSurname = new JLabel("Surname:");
		lblSurname.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSurname.setBounds(126, 125, 65, 29);
		frame.getContentPane().add(lblSurname);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPassword.setBounds(126, 223, 65, 29);
		frame.getContentPane().add(lblPassword);

		fname = new JTextField();
		fname.setFont(new Font("Tahoma", Font.PLAIN, 12));
		fname.setBounds(225, 82, 218, 25);
		frame.getContentPane().add(fname);
		fname.setColumns(10);

		lname = new JTextField();
		lname.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lname.setColumns(10);
		lname.setBounds(225, 128, 218, 25);
		frame.getContentPane().add(lname);

		email = new JTextField();
		email.setFont(new Font("Tahoma", Font.PLAIN, 12));
		email.setColumns(10);
		email.setBounds(225, 175, 218, 25);
		frame.getContentPane().add(email);

		password = new JTextField();
		password.setFont(new Font("Tahoma", Font.PLAIN, 12));
		password.setColumns(10);
		password.setBounds(225, 226, 218, 25);
		frame.getContentPane().add(password);

		JLabel lblNewLabel_1 = new JLabel("Student Registration");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblNewLabel_1.setBounds(10, 22, 564, 44);
		frame.getContentPane().add(lblNewLabel_1);
		JButton btnNewButton = new JButton("Register");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(fname.getText().length()==0)  
				      JOptionPane.showMessageDialog(null, "Firstname Required");
				   else if(lname.getText().length()==0)  
				      JOptionPane.showMessageDialog(null, "Lastname Required");
				   else if(email.getText().length()==0)  
					      JOptionPane.showMessageDialog(null, "Email Required");
				   else if(password.getText().length()==0)  
					      JOptionPane.showMessageDialog(null, "Password Required");
				   else{
				String fname1 = fname.getText(); 
				String lname1 = lname.getText(); 
				String email1 = email.getText(); 
				String password1 = password.getText(); 
				int type_id = 4;//4 = Student - Only Student can Register
				Student temp=new Student(-1, fname1, lname1, email1, password1);
				boolean result=temp.register();
				if(result)
				{
					JOptionPane.showMessageDialog(null, "You are now Registered on our System!");
					Authentication Authentication = new Authentication();
					Authentication.frame.setVisible(true);
					frame.dispose();
				}
				//Register(fname1,lname1,email1,password1,type_id);
			}}
		});

		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.setBounds(126, 285, 130, 37);
		frame.getContentPane().add(btnNewButton);
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		btnExit.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnExit.setBounds(227, 350, 130, 37);
		frame.getContentPane().add(btnExit);
		
		JButton btnLogIn = new JButton("Or Log In");
		btnLogIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Authentication Authentication = new Authentication();
				Authentication.frame.setVisible(true);
				frame.dispose();
			}
		});
		btnLogIn.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnLogIn.setBounds(313, 285, 130, 37);
		frame.getContentPane().add(btnLogIn);
	}

}