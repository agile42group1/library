package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.mysql.jdbc.PreparedStatement;

import javax.swing.JButton;

import utilise.Student;
import utilise.Administrator;
import utilise.User;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;

import javax.swing.SwingConstants;
import java.awt.Color;

public class Authentication {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Authentication window = new Authentication();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private static final String Onclick = null;
	public  JFrame frame;
	private JTextField emailFld;
	private JTextField passFld;
	

	/**
	 * Launch the application.
	 */

	/**
	 * Create the application.
	 */
	public Authentication() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(153, 204, 204));
		frame.setBounds(100, 100, 600, 448);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEmail.setBounds(130, 104, 67, 31);
		frame.getContentPane().add(lblEmail);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPassword.setBounds(130, 171, 77, 30);
		frame.getContentPane().add(lblPassword);

		emailFld = new JTextField();
		emailFld.setBounds(235, 109, 214, 25);
		frame.getContentPane().add(emailFld);
		emailFld.setColumns(32);

		passFld = new JTextField();
		passFld.setBounds(235, 176, 214, 25);
		frame.getContentPane().add(passFld);
		passFld.setColumns(32);

		JButton btnLogIn = new JButton("Log In");
		btnLogIn.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnLogIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String email=emailFld.getText();
				String password=passFld.getText();
				User temp=new User();
				int state=temp.login(email, password);
				if(state==3)
				{
					Administrator admin=new Administrator(email,password);
					admin.readAdministrator();
					AdministratorHome ah=new AdministratorHome(admin);
					ah.frame.setVisible(true);
					frame.dispose();
				}
				else if(state==4)
				{
					Student stu=new Student(email,password);
					stu.readStudent();
					StudentHome sh=new StudentHome(stu);
					sh.frame.setVisible(true);
					frame.dispose();
				}
				else if(state==-1)
				{
					
				}
			}
		});
		btnLogIn.setBounds(130, 267, 130, 37);
		frame.getContentPane().add(btnLogIn);

		JLabel lblWelcome = new JLabel("Welcome !");
		lblWelcome.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcome.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblWelcome.setBounds(10, 29, 564, 43);
		frame.getContentPane().add(lblWelcome);

		JButton btnRegister = new JButton("Or Register");
		btnRegister.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				RegisterGUI register=new RegisterGUI();
				register.frame.setVisible(true);
				frame.dispose();
			}
		});
		btnRegister.setBounds(319, 267, 130, 37);
		frame.getContentPane().add(btnRegister);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnExit.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnExit.setBounds(235, 343, 130, 37);
		frame.getContentPane().add(btnExit);

	}
}
