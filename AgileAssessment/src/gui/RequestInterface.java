package gui;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Point;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;

import utilise.Administrator;
import utilise.ResultTableModel;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class RequestInterface {

	public JFrame frame;
	private JTable table;
	private JScrollPane scrollPane;
	private ResultTableModel tableModel;
	private Administrator currentAdmin;
	private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					RequestInterface window = new RequestInterface();
//					window.frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the application.
	 * @param tableModel2 
	 */
	public RequestInterface(Administrator ad, ResultTableModel tableModel2) {
		this.currentAdmin = currentAdmin;
		initialize();
		this.tableModel = tableModel2;
		table.setModel(tableModel);
		tableModel.fireTableChanged(null);
		
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(153, 204, 204));
		frame.setBounds(100, 100, 730, 540);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		tableModel = new ResultTableModel();
		table = new JTable(tableModel);
		table.setPreferredScrollableViewportSize(new Dimension(200,200));
		scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setMaximumSize(new Dimension(100, 100));
		scrollPane.setLocation(new Point(100, 100));
		scrollPane
				.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		scrollPane.setBounds(49, 103, 610, 319);

		scrollPane.setVisible(true);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(scrollPane);
		
		btnNewButton = new JButton("Back to Main");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//AdministratorHome AdministratorHome = new AdministratorHome(currentAdmin);
				//AdministratorHome.frame.setVisible(true);
				frame.dispose();
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.setBounds(49, 433, 150, 42);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblRequestInterface = new JLabel("Request Interface");
		lblRequestInterface.setHorizontalAlignment(SwingConstants.CENTER);
		lblRequestInterface.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblRequestInterface.setBounds(10, 35, 694, 39);
		frame.getContentPane().add(lblRequestInterface);
	}
}
