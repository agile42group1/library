package utilise;

import java.sql.Connection;

import javax.swing.JOptionPane;

import utilise.Administrator;
import utilise.DAO;
import utilise.Student;

public class User {

	public int login(String email,String password)
	{
		int statement=0;;
		if (email.length() == 0)
			JOptionPane.showMessageDialog(null,
					"Empty enter detected ! Please fill up all fields");
		else if (password.length() == 0)
			JOptionPane.showMessageDialog(null,
					"Empty enter detected ! Please fill up all fields");
		else {
			if (DAO.instance.loginToDB(email, password)) {
				int userType = DAO.instance.userType(email, password);
				if (userType == 3) {
					//Administrator admin = new Administrator();
					//admin.frmCollegeLibraryAdministrator
							//.setVisible(true);
					statement=3;
				} else if (userType == 4) {
					//currentStudent = new Student(email, password);
					//readStudent(email, password);
					//StudentHome sh = new StudentHome(currentStudent);
					//sh.frame.setVisible(true);
					statement= 4;
				}
				
			} else {
				JOptionPane.showMessageDialog(null, "login failed");
				statement= -1;
			}
		}
		//if login successfully, return the user type(3 or 4).-----------
		//return -1 represents the user login failed-----------
		//return -1;-------------
		//if login successfully, page jump to student's home page or administrator's home page.
		//student's home page and adminstrator's home page not defined now.
		//if login failly, report a error.
		return statement;
	}

}
