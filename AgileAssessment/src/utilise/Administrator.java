package utilise;

import exception.DuplicateISBNException;
import exception.WrongISBNFormatException;
import exception.WrongQuantityNumberException;

public class Administrator {

	private int userId;
	private String name;
	private String surname;
	private String email;
	private String password;

	public Administrator(String e,String p)
	{
		this.email=e;
		this.password=p;
	}
	
	public Administrator(int uId, String n, String sn, String e, String p) {
		userId = uId;
		name = n;
		surname = sn;
		email = e;
		password = p;
	}
	public void setName(String name)
	{
		this.name=name;
	}
	public void setSurname(String surname)
	{
		this.surname=surname;
	}
	public void setUserId(int userId)
	{
		this.userId=userId;
	}
	public int getUserId()
	{
		return this.userId;
	}
	public String getFullName()
	{
		return name+" "+surname;
	}
	
	
	public ResultTableModel viewRequest(){
		return DAO.instance.ViewRequest();
	}
	
	//The table model represent the search result of user
	public ResultTableModel search(String key)
	{
		
		return DAO.instance.searchFromDB(key);
	}
	
	public boolean returnEdition(int editionId)
	{
		boolean result=DAO.instance.returnEditionToDB(editionId);
		return result;
	}
	
	public boolean addEdition(Edition newEdition) throws DuplicateISBNException, WrongISBNFormatException, WrongQuantityNumberException 
	{
		boolean result=DAO.instance.addEditionToDB(newEdition);
		return result;
	}
	
	public boolean updateEdition(Edition updateEdition) throws DuplicateISBNException, WrongISBNFormatException, WrongQuantityNumberException 
	{
		boolean result=DAO.instance.updateEditionToDB(updateEdition);
		return result;
	}
	
	public boolean readAdministrator()
	{
		boolean result=DAO.instance.readAdministratorFromDB(email, password, this);
		return result;
	}
}
