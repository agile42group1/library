package utilise;

public class Edition {

	private int editionId;
	private String eISBN;
	private String eTitle;
	private String author;
	private String publisher;
	private String datePublished;
	private String category;
	private int qunatityAvail;
	private int quantityTotal;
	private int typeId;
	private int userId;
	public int getEditionId() {
		return editionId;
	}
	public void setEditionId(int editionId) {
		this.editionId = editionId;
	}
	public String geteISBN() {
		return eISBN;
	}
	public void seteISBN(String eISBN) {
		this.eISBN = eISBN;
	}
	public String geteTitle() {
		return eTitle;
	}
	public void seteTitle(String eTitle) {
		this.eTitle = eTitle;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getDatePublished() {
		return datePublished;
	}
	public void setDatePublished(String datePublished) {
		this.datePublished = datePublished;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public int getQunatityAvail() {
		return qunatityAvail;
	}
	public void setQunatityAvail(int qunatityAvail) {
		this.qunatityAvail = qunatityAvail;
	}
	public int getQuantityTotal() {
		return quantityTotal;
	}
	public void setQuantityTotal(int quantityTotal) {
		this.quantityTotal = quantityTotal;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
}
