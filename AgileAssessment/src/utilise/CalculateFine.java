package utilise;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import testCase.A00211953.CalculateFineExceptionHandler;

public class CalculateFine {

	private long fine;

	public boolean Calculate(Date date2) throws CalculateFineExceptionHandler, ParseException {

		SimpleDateFormat returnDate = new SimpleDateFormat("yyyy-MM-dd");
		// For integration purposes
		// date2 = textfield2.getDate2

		Date today = new Date();
		Date date1 = returnDate.parse(returnDate.format(today));// Only for test
																// purposes
		// Date date1 = new Date(); //Use this

		long difference = date1.getTime() - date2.getTime();
		long days = (difference) / (1000 * 60 * 60 * 24);
		fine = days * 2;

		if (days < 0) {
			System.out.println("No Fine Due");
			throw new CalculateFineExceptionHandler("No Fine Due");
		}
		if (days == 0) {
			System.out.println("An edition is due for return");
			throw new CalculateFineExceptionHandler("An edition is due for return");
		}
		if (days > 0) {
			System.out.println("�" + fine);
		}
		return true;
	}
	
	public long getFine()
	{
		return fine;
	}
}