package utilise;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;

public class ResultTableModel extends AbstractTableModel {

	// each attribute of the vector represents one row
	// that's one attribute
	Vector modelTable; // used to store the value of model data that returned by
						// select statement'

	int colCount;

	String[] headers; // used to record the column name of the table

	String[] record; // used to record the current row

	public ResultTableModel() {
		modelTable = new Vector();
	}

	public ResultTableModel(ResultSet rs) {
		try {
			ResultSetMetaData metaData = rs.getMetaData();
			modelTable = new Vector();
			colCount = metaData.getColumnCount();
			headers = new String[colCount];

			for (int i = 0; i < colCount; i++) {
				headers[i] = metaData.getColumnLabel(i + 1);
			}

			while (rs.next()) {
				record = new String[colCount];
				for (int i = 0; i < colCount; i++) {
					record[i] = rs.getString(i + 1);
				}
				modelTable.addElement(record);
			}
			fireTableChanged(null);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String getColumnName(int index) {
		return headers[index];
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return colCount;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return modelTable.size();
	}

	@Override
	public Object getValueAt(int row, int col) {
		// TODO Auto-generated method stub
		// firstly located to the specified row
		// Get the value of the row, it's a String array actually
		// then return the specified column attribute
		// System.out.println("测试"+((String[])(modelTable.elementAt(row)))[col]+"测试");
		return ((String[]) (modelTable.elementAt(row)))[col];
	}

	public void refreshFromDB(Statement stmt1) {

	}
}
