package utilise;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import exception.UnavailableRequestException;
import exception.WrongRequestException;

public class Student {
	private int userId;
	private String name;
	private String surname;
	private String email;
	private String password;

	public Student(String e,String p)
	{
		this.email=e;
		this.password=p;
	}
	
	public Student(int uId, String n, String sn, String e, String p) {
		userId = uId;
		name = n;
		surname = sn;
		email = e;
		password = p;
	}
	public void setName(String name)
	{
		this.name=name;
	}
	public void setSurname(String surname)
	{
		this.surname=surname;
	}
	public void setUserId(int userId)
	{
		this.userId=userId;
	}
	public int getUserId()
	{
		return this.userId;
	}
	public String getFullName()
	{
		return name+" "+surname;
	}
	
	
	public boolean register()
	{
		//
		boolean result=DAO.instance.registerToDB(name, surname, email, password, 4);
		return result;
	}
	
	
	//The table model represent the search result of user
	public ResultTableModel search(String key)
	{
		return DAO.instance.searchFromDB(key);
	}
	
	public void placeRequest(int []editionId) throws WrongRequestException, UnavailableRequestException, ParseException
	{
	//	DAO.Utilise.instance.processRequest(editionId, userId, issueDate, expiryDate);
		Date now=new Date();
		DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
		String issueDate=df.format(now);
		
		Calendar c=Calendar.getInstance();     
		c.setTime(new Date());   
		c.add(Calendar.DATE,10);   
		Date endDate=c.getTime();   
		String endDateStr=df.format(endDate);   
		
		DAO.instance.placeRequestToDB(editionId,userId,issueDate,endDateStr);
	}
	
	
	public boolean readStudent()
	{
		boolean result=DAO.instance.readStudentFromDB(email, password, this);
		return result; 
	}
	
	public ResultTableModel searchInterlibraryRequest(String searchKey)
	{
		ResultTableModel rtm=DAO.instance.searchInterlibrary(searchKey);
		return rtm;
	}
//	
	public boolean placeInterlibraryRequest(int editionId,String isbn)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		//For integration purposes
		//date2 = textfield2.getDate2
		Date tody=new Date();
		String todayDate=sdf.format(tody);
		
		boolean result=DAO.instance.placeInterlibraryRequest(todayDate, editionId, userId);
		result=result&DAO.instance.updateInterlibraryEdition(isbn);
		return result;
	}
	
	public ResultTableModel readOverDueLoan()
	{
		ResultTableModel rtm=DAO.instance.viewFine(userId);
		return rtm;
	}
}
