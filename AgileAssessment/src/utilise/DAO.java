package utilise;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import exception.DuplicateISBNException;
import exception.UnavailableRequestException;
import exception.WrongISBNFormatException;
import exception.WrongQuantityNumberException;
import exception.WrongRequestException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public enum DAO {

	instance;
	private String searchKey;

	public Connection connectDB() throws SQLException {

		Connection con = null;
		try {
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url = "jdbc:mysql://localhost:3306/collegeLibraryDB";
			// Connect to DB using DB URL, user name and password
			con = DriverManager.getConnection(url, "root", "admin");
		} catch (Exception e) {
			System.out.println("Error: Failed to connect to database\n" + e.getMessage());
		}
		return con;
	}

	public boolean executeUpdate(String updateStmt) throws SQLException {

		boolean res = false;

		Connection con = connectDB();
		try {
			System.out.println(updateStmt);
			java.sql.Statement stmt = con.createStatement();
			stmt.executeUpdate(updateStmt);
			res = true;
		} catch (SQLException e) {
			System.out.println("Error: Failed to connect to database\n");
			e.printStackTrace();
		}
		return res;
	}

	public int userType(String email, String password) {
		int flag = 0;
		try {
			Connection con = connectDB();
			PreparedStatement pst = (PreparedStatement) con
					.prepareStatement("Select * from user where u_email=? and u_password=?");
			pst.setString(1, email);
			pst.setString(2, password);
			ResultSet rs = pst.executeQuery();
			// rs.get
			if (rs.next())
				flag = Integer.parseInt(rs.getString(6));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	public boolean loginToDB(String email, String password) {
		try {
			Connection con = connectDB();
			PreparedStatement pst = (PreparedStatement) con
					.prepareStatement("Select * from user where u_email=? and u_password=?");
			pst.setString(1, email);
			pst.setString(2, password);
			ResultSet rs = pst.executeQuery();
			if (rs.next())
				return true;
			else
				return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean registerToDB(String fname1, String lname1, String email1, String password1, int type_id) {
		try {
			// Class.forName("com.mysql.jdbc.Driver"); // MySQL database
			// connection
			// Connection conn =
			// DriverManager.getConnection("jdbc:mysql://localhost:3306/collegeLibraryDB"
			// , "root", "admin");
			// String sql = "INSERT INTO Users (username, password, fullname,
			// email) VALUES (?, ?, ?, ?)";
			Connection con = connectDB();
			PreparedStatement pst = (PreparedStatement) con.prepareStatement(
					"INSERT INTO User (u_name, u_surname, u_email, u_password, type_id) VALUES (?, ?, ?, ?, ?)");
			// PreparedStatement pst = conn.prepareStatement(sql);
			pst.setString(1, fname1);
			pst.setString(2, lname1);
			pst.setString(3, email1);
			pst.setString(4, password1);
			pst.setLong(5, type_id);
			int rowsInserted = pst.executeUpdate();
			if (rowsInserted > 0)
				return true;
			else
				return false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean placeRequestToDB(int[] editionId, int userId, String issueDate, String expiryDate)
			throws WrongRequestException, UnavailableRequestException, ParseException {

		// check if the request is available
		boolean updatedLoan = false;
		boolean available = false;
		boolean result = false;
		boolean decreased = false;
		boolean updatedRequest = false;
		boolean updateItem = false;
		for (int i = 0; i < editionId.length; i++) {
			if (editionId[i] <= 0) {
				throw new WrongRequestException("Wrong Request Exception");
			}
			// the wrong request exception will be thrown inside the
			// requestIsAvaliable method
			if (!requestIsAvailable(editionId[i])) {
				throw new UnavailableRequestException("Unavailable Request Exception");
			}
		}

		if (userId <= 0) {
			throw new WrongRequestException("Wrong Request Exception");
		}
		// check if the user exists into the database
		if (!userExists(userId)) {
			throw new WrongRequestException("Wrong Request Exception");
		}
		dateCheck(issueDate, expiryDate);

		// add a new entry into loan table
		updatedLoan = processLoanTable(issueDate, expiryDate, userId);
		int loanId = getLoanId();
		for (int i = 0; i < editionId.length; i++) {
			// if the request is available, do some operation
			decreased = decreaseAmount(editionId[i]);
			updateItem = processItemTable(loanId, editionId[i]);
			// add a new request entry into the database request table
			updatedRequest = processRequestTable(issueDate, editionId[i], userId);
			result = updatedLoan && updatedRequest && decreased && updateItem;
			if (!result) {
				return result;
			}
		}

		return result;
	}

	// responsible for search the target books
	public ResultTableModel searchFromDB(String searchKey) {
		this.searchKey = searchKey;
		Connection con = null;
		ResultSet rs = null;
		ResultTableModel results = null;
		try {
			con = connectDB();
			PreparedStatement psmt = con.prepareStatement("select * from edition where e_isbn=? or e_title=? ");

			// set the parameter of select statement
			psmt.setString(1, searchKey);
			psmt.setString(2, searchKey);

			// execute the search
			rs = psmt.executeQuery();
			results = new ResultTableModel(rs);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return results;
	}

	// responsible for interlibrary search the target editions
	public ResultTableModel searchInterlibrary(String searchKey) {
		this.searchKey = searchKey;
		Connection con = null;
		ResultSet rs = null;
		ResultTableModel results = null;
		try {
			con = connectDB();
			PreparedStatement psmt = con
					.prepareStatement("select * from inter_edition where inter_e_isbn=? or inter_e_title=? ");

			// set the parameter of select statement
			psmt.setString(1, searchKey);
			psmt.setString(2, searchKey);

			// execute the search
			rs = psmt.executeQuery();
			results = new ResultTableModel(rs);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return results;
	}

	public boolean addEditionToDB(Edition newEdition) throws WrongISBNFormatException, WrongQuantityNumberException {
		boolean result = false;
		Statement stmt = null;
		Connection con = null;
		try {
			con = connectDB();
			stmt = con.createStatement();

			String eISBN = newEdition.geteISBN();
			String title = newEdition.geteTitle();
			String author = newEdition.getAuthor();
			String publisher = newEdition.getPublisher();
			String datePublished = newEdition.getDatePublished();
			String category = newEdition.getCategory();
			int quantityAvail = newEdition.getQunatityAvail();
			int quantityTotal = newEdition.getQuantityTotal();
			int typeId = newEdition.getTypeId();
			int userId = newEdition.getUserId();

			String insertStmt = "INSERT INTO edition VALUES(" + null + ",'" + eISBN + "','" + title + "','" + author
					+ "','" + publisher + "','" + datePublished + "','" + category + "','" + quantityAvail + "','"
					+ quantityTotal + "','" + typeId + "'," + userId + ");";
			int effectedRows = stmt.executeUpdate(insertStmt);
			if (effectedRows == 1) {
				result = true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				con = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				stmt = null;
			}
		}
		return result;
	}

	public boolean updateEditionToDB(Edition updatedEdition)
			throws DuplicateISBNException, WrongISBNFormatException, WrongQuantityNumberException {
		boolean result = false;
		Statement stmt = null;
		Connection con = null;
		try {
			con = connectDB();
			stmt = con.createStatement();

			int editionId = updatedEdition.getEditionId();
			String eISBN = updatedEdition.geteISBN();

			boolean existsISBN = checkISBN(eISBN);
			if (existsISBN) {
				throw new DuplicateISBNException("Duplicate ISBN");
			}
			String author = updatedEdition.getAuthor();
			String title = updatedEdition.geteTitle();
			String publisher = updatedEdition.getPublisher();
			int quantityAvail = updatedEdition.getQunatityAvail();
			int quantityTotal = updatedEdition.getQuantityTotal();

			checkQuantityNum(quantityAvail, quantityTotal);
			String datePublished = updatedEdition.getDatePublished();
			int userId = updatedEdition.getUserId();
			int typeId = updatedEdition.getTypeId();

			String updateStmt = "UPDATE EDITION SET E_ISBN='" + eISBN + "',E_AUTHOR='" + author + "',E_TITLE='" + title
					+ "',E_PUBLISHER='" + publisher + "',E_QUANTITY_AVAIL=" + quantityAvail + ",E_QUANTITY_TOTAL="
					+ quantityTotal + ",E_DATE_PUBLISHED='" + datePublished + "' WHERE  EDITION_ID=" + editionId;
			System.out.println(updateStmt);
			int effectedRows = stmt.executeUpdate(updateStmt);
			if (effectedRows == 1) {
				result = true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				con = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				stmt = null;
			}
		}
		return result;
	}

	public boolean requestIsAvailable(int editionId) throws WrongRequestException {
		String queryStmt = "SELECT e_quantity_avail FROM edition WHERE edition_id=" + editionId;
		Connection con = null;
		boolean result = false;
		try {
			con = connectDB();
			Statement stmt = con.createStatement();

			ResultSet rs = stmt.executeQuery(queryStmt);
			int quantityAvail = 0;
			if (rs.next()) {
				quantityAvail = rs.getInt(1);
			} else {
				throw new WrongRequestException("Wrong Request Exception");
			}
			if (quantityAvail > 0) {
				result = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}

	public boolean userExists(int userId) {
		String queryStmt = "SELECT * FROM user WHERE user_id=" + userId;
		Connection con = null;
		boolean result = false;
		try {
			con = connectDB();
			Statement stmt = con.createStatement();

			ResultSet rs = stmt.executeQuery(queryStmt);
			if (rs.next()) {
				result = true;
			} else {
				result = false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}

	public boolean dateCheck(String issueDate, String expiryDate) throws WrongRequestException, ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setLenient(false); // 是否是严格的匹配

		Date iDate = sdf.parse(issueDate);
		Date eDate = sdf.parse(expiryDate);
		if (iDate.compareTo(eDate) > 0) {
			throw new WrongRequestException("Wrong Request Exception");
		}
		return true;
	}

	// public boolean executeUpdate(String updateStmt) {
	// // update the edition table in database
	// boolean result = false;
	// // Connection con = getConnection();
	// // try {
	// // Statement stmt = con.createStatement();
	// // stmt.executeUpdate(updateStmt);
	// // result = true;
	// // } catch (SQLException e) {
	// // result = false;
	// // } finally {
	// // try {
	// // con.close();
	// // } catch (SQLException e) {
	// // // TODO Auto-generated catch block
	// // e.printStackTrace();
	// // }
	// // }
	// return result;
	// }

	public boolean processLoanTable(String issueDate, String expiryDate, int userId) {
		String updateStmt = "INSERT INTO loan(loan_id,user_id,l_issue_date,l_expiry_date) VALUES(NULL," + userId + ",'"
				+ issueDate + "','" + expiryDate + "')";
		// System.out.println(updateStmt);
		boolean updated = false;
		try {
			updated = executeUpdate(updateStmt);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return updated;
	}

	public int getLoanId() {
		ResultSet rs = null;
		Connection con = null;
		int loanId = 0;
		try {
			con = connectDB();
			Statement stmt = con.createStatement();
			String queryStmt = "SELECT max(loan_id) FROM LOAN";
			rs = stmt.executeQuery(queryStmt);
			while (rs.next()) {
				loanId = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return loanId;
	}

	// decrease the quantity of edition
	public boolean decreaseAmount(int id) throws WrongRequestException, UnavailableRequestException {
		// update the edition table in database

		boolean result = false;
		if (requestIsAvailable(id)) {
			String updateStmt = "update edition set e_quantity_avail=e_quantity_avail-1 where edition_id=" + id;
			try {
				result = executeUpdate(updateStmt);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			throw new UnavailableRequestException("Unavailable Request Exception");
		}
		return result;
	}

	public boolean processItemTable(int loanId, int editionId) {
		String updateStmt = "INSERT INTO item(item_id,loan_id,edition_id) VALUES(NULL," + loanId + "," + editionId
				+ ");";
		boolean updated = false;
		try {
			updated = executeUpdate(updateStmt);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return updated;
	}

	public boolean processRequestTable(String date, int editionId, int userId) {
		// INSERT INTO request(request_id,r_timestamp,edition_id,user_id)
		// VALUES(NULL,'2017-3-29',1,2);
		String updateStmt = "INSERT INTO request(request_id,r_timestamp,edition_id,user_id) VALUES(NULL,'" + date + "',"
				+ editionId + "," + userId + ");";
		boolean updated = false;
		try {
			updated = executeUpdate(updateStmt);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(updated);
		return updated;
	}

	public boolean placeInterlibraryRequest(String date, int interEditionId, int userId) {

		String insertStmt = "INSERT INTO inter_request(inter_request_id, inter_r_timestamp, inter_e_id, user_id) VALUES(NULL,'"
				+ date + "'," + interEditionId + "," + userId + ");";
		boolean insertedILR = false;
		try {
			insertedILR = executeUpdate(insertStmt);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(insertedILR);
		return insertedILR;

	}

	public boolean updateInterlibraryEdition(String inter_e_isbn) {

		String updateStmt = "UPDATE inter_edition SET inter_e_quantity_avail = inter_e_quantity_avail-1 WHERE inter_e_isbn="
				+ inter_e_isbn;
		boolean updatedILE = false;
		try {
			updatedILE = executeUpdate(updateStmt);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(updatedILE);
		return updatedILE;
	}

	public boolean checkISBN(String isbn) throws WrongISBNFormatException {
		if (isbn.length() != 14) {
			throw new WrongISBNFormatException("Wrong ISBN format, the ISBN should be 14 length number array");
		}
		for (int i = 0; i < isbn.length(); i++) {
			char c = isbn.charAt(i);
			if (c < '0' || c > '9') {
				throw new WrongISBNFormatException("Wrong ISBN format, the ISBN should be 14 length number array");
			}
		}
		boolean result = false;
		Connection con = null;
		Statement stmt = null;
		try {
			con = connectDB();
			stmt = con.createStatement();
			String queryStmt = "SELECT * FROM EDITION WHERE E_ISBN='" + isbn + "'";
			ResultSet rs = stmt.executeQuery(queryStmt);
			result = rs.next();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				con = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				stmt = null;
			}
		}
		return result;
	}

	public void checkQuantityNum(int quantityAvail, int quantityTotal) throws WrongQuantityNumberException {
		if (quantityAvail < 0 || quantityTotal < 0) {
			throw new WrongQuantityNumberException("Wrong quantity number");
		}
		if (quantityAvail > quantityTotal) {
			throw new WrongQuantityNumberException("Wrong quantity number");
		}
	}

	public boolean readStudentFromDB(String email, String password, Student currentStudent) {

		boolean result = false;
		Connection conn = null;
		try {
			conn = connectDB();
			PreparedStatement pst = (PreparedStatement) conn
					.prepareStatement("Select * from user where u_email=? and u_password=?");
			pst.setString(1, email);
			pst.setString(2, password);
			ResultSet rs = pst.executeQuery();
			// rs.get
			if (rs.next()) {
				int userId = Integer.parseInt(rs.getString(1));
				String userName = rs.getString(2);
				String surName = rs.getString(3);
				currentStudent.setName(userName);
				currentStudent.setSurname(surName);
				currentStudent.setUserId(userId);
				result = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return result;
	}

	public boolean readAdministratorFromDB(String email, String password, Administrator currentAdministrator) {

		boolean result = false;
		Connection conn = null;
		try {
			conn = connectDB();
			PreparedStatement pst = (PreparedStatement) conn
					.prepareStatement("Select * from user where u_email=? and u_password=?");
			pst.setString(1, email);
			pst.setString(2, password);
			ResultSet rs = pst.executeQuery();
			// rs.get
			if (rs.next()) {
				int userId = Integer.parseInt(rs.getString(1));
				String userName = rs.getString(2);
				String surName = rs.getString(3);
				currentAdministrator.setName(userName);
				currentAdministrator.setSurname(surName);
				currentAdministrator.setUserId(userId);
				result = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return result;
	}

	public boolean refreshTableModel(ResultTableModel tm) {

		tm.modelTable = new Vector();
		boolean result = false;
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try {
			con = connectDB();
			stmt = con.createStatement();
			// Execute the query and store the result set and its metadata
			rs = stmt.executeQuery("SELECT * FROM edition_view");
			ResultSetMetaData meta = rs.getMetaData();

			// to get the number of columns
			tm.colCount = meta.getColumnCount();
			// Now must rebuild the headers array with the new column names
			tm.headers = new String[tm.colCount];

			for (int h = 0; h < tm.colCount; h++) {
				tm.headers[h] = meta.getColumnName(h + 1);
			} // end for loop

			// fill the cache with the records from the query, ie get all the
			// rows

			while (rs.next()) {
				tm.record = new String[tm.colCount];
				for (int i = 0; i < tm.colCount; i++) {
					tm.record[i] = rs.getString(i + 1);
				} // end for loop
				tm.modelTable.addElement(tm.record);
			} // end while loop
			tm.fireTableChanged(null);
			result = true;
		} // end try clause
		catch (Exception e) {
			System.out.println("Error with refreshFromDB Method\n" + e.toString());
			e.printStackTrace();
		} // end catch clause to query table
		finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return result;
	}

	public boolean refreshInterEdition(ResultTableModel tm) {

		tm.modelTable = new Vector();
		boolean result = false;
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try {
			con = connectDB();
			stmt = con.createStatement();
			// Execute the query and store the result set and its metadata
			rs = stmt.executeQuery("SELECT * FROM inter_edition");
			ResultSetMetaData meta = rs.getMetaData();

			// to get the number of columns
			tm.colCount = meta.getColumnCount();
			// Now must rebuild the headers array with the new column names
			tm.headers = new String[tm.colCount];

			for (int h = 0; h < tm.colCount; h++) {
				tm.headers[h] = meta.getColumnName(h + 1);
			} // end for loop

			// fill the cache with the records from the query, ie get all the
			// rows

			while (rs.next()) {
				tm.record = new String[tm.colCount];
				for (int i = 0; i < tm.colCount; i++) {
					tm.record[i] = rs.getString(i + 1);
				} // end for loop
				tm.modelTable.addElement(tm.record);
			} // end while loop
			tm.fireTableChanged(null);
			result = true;
		} // end try clause
		catch (Exception e) {
			System.out.println("Error with refreshFromDB Method\n" + e.toString());
			e.printStackTrace();
		} // end catch clause to query table
		finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return result;
	}

	public ArrayList<NotificationInfo> getRequestInfo() {
		ArrayList<NotificationInfo> resultsNoti = new ArrayList<NotificationInfo>(0);
		try {
			Connection con = connectDB();
			PreparedStatement pst = (PreparedStatement) con.prepareStatement("select * from inter_request");
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				int requestId = rs.getInt("INTER_E_ID");
				NotificationInfo info = new NotificationInfo();
				int userId = rs.getInt("USER_ID");
				readInterRequestUserInfo(info, userId);
				interLoan(info, requestId);
				resultsNoti.add(info);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultsNoti;
	}

	public void interLoan(NotificationInfo noti, int loanId)// try to get the
															// edition title
															// from
															// inter_request
															// table
	{
		boolean result = false;
		Connection con = null;
		Statement stmt = null;
		try {
			con = connectDB();
			stmt = con.createStatement();
			String retriveLoans = "SELECT * FROM INTER_EDITION WHERE INTER_E_ID=" + loanId;
			ResultSet rsLoans = stmt.executeQuery(retriveLoans);
			if (rsLoans.next()) {
				String title = rsLoans.getString("INTER_E_TITLE");
				noti.setEditionName(title);
			} else {
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public ArrayList<NotificationInfo> retriExpiryDateNoti() {
		ArrayList<NotificationInfo> resultsNoti = new ArrayList<NotificationInfo>(0);
		Connection con = null;
		Statement stmt = null;
		try {
			con = connectDB();
			stmt = con.createStatement();
			String retriveItem = "SELECT * FROM ITEM";
			ResultSet rsItems = stmt.executeQuery(retriveItem);
			Date today = new Date();
			while (rsItems.next()) {
				int loanId = rsItems.getInt("lOAN_ID");
				boolean notifyLoan = notifiedLoan(loanId);
				if (notifyLoan) {
					NotificationInfo temp = new NotificationInfo();
					int editionId = rsItems.getInt("EDITION_ID");
					readUserInfo(temp, loanId);
					readEditionInfo(temp, editionId);

					resultsNoti.add(temp);
				}
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultsNoti;
	}

	public boolean notifiedLoan(int loanId) {
		boolean result = false;
		Connection con = null;
		Statement stmt = null;
		try {
			con = connectDB();
			stmt = con.createStatement();
			String retriveLoans = "SELECT * FROM LOAN WHERE LOAN_ID=" + loanId;
			ResultSet rsLoans = stmt.executeQuery(retriveLoans);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date today = new Date();
			today = sdf.parse(sdf.format(today));
			System.out.println(sdf.format(today));
			if (rsLoans.next()) {
				Date date = rsLoans.getDate("L_EXPIRY_DATE");
				date = sdf.parse(sdf.format(date));
				System.out.println(sdf.format(date));
				if (today.compareTo(date) <= 0) {
					if ((date.getTime() - today.getTime()) / 1000 / 60 / 60 / 24 < 2) {
						result = true;
					}
				}
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}

	public void readUserInfo(NotificationInfo temp, int loanId) {
		Connection con = null;
		Statement stmt = null;

		try {
			con = connectDB();
			stmt = con.createStatement();

			// get the expiry date
			String loanInfo = "SELECT * FROM LOAN WHERE LOAN_ID=" + loanId;
			ResultSet loanInfoRS = stmt.executeQuery(loanInfo);
			if (loanInfoRS.next()) {
				String expiryDate = loanInfoRS.getString("l_expiry_date");
				temp.setExpiryDate(expiryDate);
				// get the user name according to user id
				int userId = loanInfoRS.getInt("USER_ID");
				temp.setUserId(userId);
				String userInfo = "SELECT * FROM USER WHERE USER_ID=" + userId;
				ResultSet userInfoRS = stmt.executeQuery(userInfo);
				if (userInfoRS.next()) {
					String firstName = userInfoRS.getString("U_NAME");
					String lastName = userInfoRS.getString("U_SURNAME");
					String fullName = firstName + " " + lastName;
					String emailAddr = userInfoRS.getString("U_EMAIL");
					temp.setUserName(fullName);
					temp.setEmail(emailAddr);
				}
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	public void readInterRequestUserInfo(NotificationInfo temp, int userId) {
		Connection con = null;
		Statement stmt = null;

		try {
			con = connectDB();
			stmt = con.createStatement();

			// get the expiry date

			String userInfo = "SELECT * FROM USER WHERE USER_ID=" + userId;
			ResultSet userInfoRS = stmt.executeQuery(userInfo);
			if (userInfoRS.next()) {
				String firstName = userInfoRS.getString("U_NAME");
				String lastName = userInfoRS.getString("U_SURNAME");
				String fullName = firstName + " " + lastName;
				String emailAddr = userInfoRS.getString("U_EMAIL");
				temp.setUserName(fullName);
				temp.setEmail(emailAddr);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	public ResultTableModel ViewRequest() {

		Connection con = null;
		ResultSet rs = null;
		ResultTableModel results = null;
		try {
			con = connectDB();

			PreparedStatement psmt = con.prepareStatement(
					"SELECT user.u_name, user.u_surname,user.user_id, edition.e_title,request.r_timestamp,edition.e_isbn,request.request_id FROM user,edition,request where request.user_id=user.user_id");

			rs = psmt.executeQuery();
			results = new ResultTableModel(rs);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return results;

	}

	public void readEditionInfo(NotificationInfo temp, int editionId) {

		Connection con = null;
		Statement stmt = null;
		try {
			con = connectDB();
			stmt = con.createStatement();
			// read the edition ISBN and edition title accroding to the editon
			// id
			String editionInfo = "SELECT * FROM EDITION WHERE EDITION_ID=" + editionId;
			ResultSet editionInfoRS = stmt.executeQuery(editionInfo);
			if (editionInfoRS.next()) {
				String isbn = editionInfoRS.getString("E_ISBN");
				temp.seteISBN(isbn);
				String editionName = editionInfoRS.getString("E_TITLE");
				temp.setEditionName(editionName);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public boolean returnEditionToDB(int editionId) {
		boolean result = false;
		Statement stmt = null;
		Connection con = null;
		try {
			con = connectDB();
			stmt = con.createStatement();

			String updateStmt = "UPDATE EDITION SET E_QUANTITY_AVAIL= E_QUANTITY_AVAIL+1 WHERE EDITION_ID = "
					+ editionId;
			System.out.println(updateStmt);
			int effectedRows = stmt.executeUpdate(updateStmt);
			if (effectedRows == 1) {
				result = true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				con = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				stmt = null;
			}
		}
		return result;
	}

	public ArrayList<String> getAllStudentEmails() {
		ArrayList<String> result = new ArrayList<String>(0);
		Connection con = null;
		Statement stmt = null;
		try {
			con = connectDB();
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT U_EMAIL FROM USER WHERE TYPE_ID=4");
			while (rs.next()) {
				result.add(rs.getString(1));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				con = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				stmt = null;
			}
		}
		return result;
	}

	public ResultTableModel viewFine(int userId) {

		Connection con = null;
		ResultSet rs = null;
		ResultTableModel results = null;
		try {
			con = connectDB();
			PreparedStatement psmt = con.prepareStatement(
					"SELECT LOAN_ID,L_ISSUE_DATE,L_EXPIRY_DATE FROM LOAN WHERE L_EXPIRY_DATE<NOW() AND USER_ID="
							+ userId);

			rs = psmt.executeQuery();
			results = new ResultTableModel(rs);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return results;

	}

}
