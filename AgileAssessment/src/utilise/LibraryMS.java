package utilise;

import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class LibraryMS {

	 
	public void timerNotify()
	{
		//schedule  the task in a background thread, true means the task is done in a low priority
		Timer timer=new Timer(true);
		
		Date time=new Date();
		time.setYear(117);
		time.setMonth(3);
		time.setDate(1);
		time.setHours(19);
		time.setMinutes(18);
		time.setSeconds(0);
		
		System.out.println(time.toString());
		//The timerTask actually is a thread
		TimerTask task=new TimerTask(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				notifyExpiryDate();
			}
		};
		timer.schedule(task,time,24*60*60*1000);
	}
	
	public void notifyExpiryDate()
	{
		ArrayList<NotificationInfo> resultsNoti=DAO.instance.retriExpiryDateNoti();
		
		//According to the return notifications send email
		for(int i=0;i<resultsNoti.size();i++)
		{
			sendEmail(resultsNoti.get(i));
		}
	}
	
	public void notifyInterlib()
	{
		ArrayList<NotificationInfo> resultsNoti=DAO.instance.getRequestInfo();
		for(int i=0; i<resultsNoti.size(); i++){
			sendNoti(resultsNoti.get(i));
		}
	}
	
	public void notifyAvailableEditions(String isbn,String title)
	{
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFacotry.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		
		Session session = Session.getDefaultInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("agilegroupb1@gmail.com", "www.ait.ie");
			}
		});

		ArrayList<String> studentsEmail=DAO.instance.getAllStudentEmails();
		
		try {
			
			for(int i=0;i<studentsEmail.size();i++)
			{
				String emailAddr=studentsEmail.get(i);
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress("agilegroupb1@gmail.com"));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailAddr));
				message.setSubject("Inter-library Request Notification");	
				String content="To all, edition:"+title+"("+isbn+") is available now, you can borrow it if you are interested in";
				message.setText(content);
				Transport.send(message);
			}	
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private void sendNoti(NotificationInfo temp) {
		// TODO Auto-generated method stub
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFacotry.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		
		Session session = Session.getDefaultInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("agilegroupb1@gmail.com", "www.ait.ie");
			}
		});

		try {
			
			String emailAddr=temp.getEmail();
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("agilegroupb1@gmail.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailAddr));
			message.setSubject("Inter-library Request Notification");
			
			String userName=temp.getUserName();
			int userId=temp.getUserId();
			//String expiryDate=temp.getExpiryDate();
			//String editionISBN=temp.geteISBN();
			String editionName=temp.getEditionName();
			
			String content="Dear "+userName+"(userID:"+userId+"), your inter-library edition request"+editionName+"is ready for you";
			message.setText(content);
			Transport.send(message);
			
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	

	public void sendEmail(NotificationInfo temp) {
		// configure the attributes of email server to send email

		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFacotry.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		// get a connection between localhost and Gmail SMTP server
		Session session = Session.getDefaultInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("agilegroupb1@gmail.com", "www.ait.ie");
			}
		});

		try {
			
			String emailAddr=temp.getEmail();
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("agilegroupb1@gmail.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailAddr));
			message.setSubject("Loan Expiry Notification");
			
			String userName=temp.getUserName();
			int userId=temp.getUserId();
			String expiryDate=temp.getExpiryDate();
			String editionISBN=temp.geteISBN();
			String editionName=temp.getEditionName();
			
			String content="Dear "+userName+"(userID:"+userId+"), your loan edition "+editionName+"(editionISBN:"+editionISBN+"), will be expiried on "+expiryDate+", please return it back on time";
			message.setText(content);
			Transport.send(message);
			
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
}
