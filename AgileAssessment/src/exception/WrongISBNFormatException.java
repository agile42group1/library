package exception;

public class WrongISBNFormatException extends Exception{

	public WrongISBNFormatException(String message)
	{
		super(message);
	}
}
