package exception;

public class UnavailableRequestException extends Exception{

	public UnavailableRequestException(String message)
	{
		super(message);
	}
}
