package exception;

public class WrongQuantityNumberException extends Exception{

	public WrongQuantityNumberException(String message)
	{
		super(message);
	}
}
