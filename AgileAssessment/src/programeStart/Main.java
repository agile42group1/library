package programeStart;



import java.awt.EventQueue;

import gui.Authentication;
import utilise.LibraryMS;

public class Main {
	public static void main(String[] args) {
		LibraryMS lms=new LibraryMS();
		lms.timerNotify();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Authentication window = new Authentication();
					window.frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}