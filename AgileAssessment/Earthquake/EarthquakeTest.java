
import org.junit.Test;

import junit.framework.TestCase;


public class EarthquakeTest extends TestCase {
	
	// Test Number: 1
	// Test Objective: To test for an earthquake greater than 8.0
	// Input(s): magnitude = 9.2
	// Expected Output(s) = "Most structures fall"
	public void testgetDescription0001() {
		
		Earthquake testObject = new Earthquake(9.2);
		assertEquals("Most structures fall", testObject.getDescription());
		
	}
	
	// Test Number: 2
	// Test Objective: To test for an earthquake greater than 3.5 but less than 4.5
	// Input(s): magnitude = 3.7
	// Expected Output(s) = "Felt by many people, no destruction"
	public void testgetDescription0002() {
		
		Earthquake testObject = new Earthquake(3.7);
		assertEquals("Felt by many people, no destruction", testObject.getDescription());
		
	}
	
	// Test Number: 3
	// Test Objective: To test for an earthquake greater than 4.5 but less than 6.0
	// Input(s): magnitude = 5.0
	// Expected Output(s) = "Damage to poorly constructed buildings"
	public void testgetDescription0003() {
		
		Earthquake testObject = new Earthquake(5.0);
		assertEquals("Damage to poorly constructed buildings", testObject.getDescription());
		
	}
	
	// Test Number: 4
	// Test Objective: To test for an earthquake greater than 0.0 but less than 3.5
	// Input(s): magnitude = 3.2
	// Expected Output(s) = "Generally not felt by people"
	public void testgetDescription0004() {
			
		Earthquake testObject = new Earthquake(3.2);
		assertEquals("Generally not felt by people", testObject.getDescription());
			
		}
	
	// Test Number: 5
	// Test Objective: To test for an earthquake greater than 6.0 but less than 7.0
	// Input(s): magnitude = 6.2
	// Expected Output(s) = "Many buildings damaged, some collapse"
	public void testgetDescription0005() {
				
		Earthquake testObject = new Earthquake(6.2);
		assertEquals("Many buildings damaged, some collapse", testObject.getDescription());
				
		}
	
	// Test Number: 6
	// Test Objective: To test for an earthquake greater than 7.0 but less than 8.0
	// Input(s): magnitude = 7.2
	// Expected Output(s) = "Many buildings destroyed"
	public void testgetDescription0006() {
					
		Earthquake testObject = new Earthquake(7.2);
		assertEquals("Many buildings destroyed", testObject.getDescription());
					
		}
	
	// Test Number: 7
	// Test Objective: Test negative input
	// Input(s): magnitude = -0.2
	// Expected Output(s) = "Negative numbers are not valid"
	public void testgetDescription0007() {
		
		Earthquake testObject = new Earthquake(-0.2);
		assertEquals("Negative numbers are not valid", testObject.getDescription());
					
		}

}
