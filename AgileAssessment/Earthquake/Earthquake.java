public class Earthquake {
	
	// Class describes the effects of an earthquake
	// (To demonstrate use of Code Coverage Tool)
	
	// Attributes
	private double richter;

	// Constructor
	// magnitude specifies the magnitude value on the Richter scale
	
	public Earthquake(double magnitude) {
		
		richter = magnitude;
	}
	
	// Method
	// Gets and returns a description of the effects of the earthquake
	
	public String getDescription() {
		
		String description;
		
		if (richter >= 8.0)
			description = "Most structures fall";
		else if (richter >= 7.0)
			description = "Many buildings destroyed";
		else if (richter >= 6.0)
			description = "Many buildings damaged, some collapse";
		else if (richter >= 4.5)
			description = "Damage to poorly constructed buildings";
		else if (richter >= 3.5)
			description = "Felt by many people, no destruction";
		else if (richter >= 0.0)
			description = "Generally not felt by people";
		else 
			description = "Negative numbers are not valid";
	
		return description;
	}

}
